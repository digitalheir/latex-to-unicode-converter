var expect = require('chai').expect;
var latexToUnicode = require('../index.js');


describe('latex-to-unicode', function() {


  describe("binoms", function() {
    it("should convert simple binoms using conventional notation", function() {
      var latex = "\\binom{1}{2}";
      var unicode = latexToUnicode(latex);
      expect(unicode).to.equal("(1 ¦ 2)");
    });

    it("should accept others LaTeX symbols", function() {
      var latex = "\\binom{\\pi}{3}";
      var unicode = latexToUnicode(latex);
      expect(unicode).to.equal("(π ¦ 3)");
    });

    it("should correctly convert nested binoms", function() {
      var latex = "\\binom{\\binom{1}{2}}{\\binom{3}{4}}";
      var unicode = latexToUnicode(latex);
      expect(unicode).to.equal("((1 ¦ 2) ¦ (3 ¦ 4))");
    });
  });

  describe("accents", function() {
    it("should being able to apply accent on single element", function() {
      var latex = "\\tilde{a}";
      var unicode = latexToUnicode(latex);
      expect(unicode).to.equal("ã");
    });

    it("should being able to apply accent on group of elements", function() {
      var latex = "\\mathring{BCD}";
      var unicode = latexToUnicode(latex);
      expect(unicode).to.equal("BC̊D");
    });
  });

});

