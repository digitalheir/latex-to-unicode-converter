import "mocha";

import {expect} from "chai";

import {newFixArg, newTeXComm, newTeXRaw, stringifyLaTeX} from "latex-parser";
import {convertLaTeX, convertLaTeXBlocks, convertLaTeXToUnicode} from "../src/convert";
import {characterUnicodeChart} from "../src/unicode/commands/0args/symbols";

describe("unicode", () => {
    it("should handle error", function () {
        expect(
            convertLaTeXBlocks({
                    translateTo: "unicode",
                    onError: (e, l) => stringifyLaTeX(l)
                }, [
                    newTeXComm("uhhhhhhhh", newFixArg([newTeXRaw("o")]))
                ]
            )
        ).to.equal(
            "\\uhhhhhhhh{o}"
        );
    });

    it("should handle argument", function () {
        expect(
            convertLaTeXBlocks({
                    translateTo: "unicode"
                }, [
                    newTeXComm('"', newFixArg([newTeXRaw("o")])),
                    newTeXRaw("o")
                ]
            )
        ).to.equal(
            "öo"
        );
    });

    it("should parse src", function () {
        expect(
            convertLaTeX({translateTo: "unicode"}, "\\\"oa")
        ).to.equal(
            "öa"
        );
    });

    describe("aliases", function () {
        it("should convert `\\mathbb{}`", function () {
            const latex = "\\mathbb{mathBB}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("𝕞𝕒𝕥𝕙𝔹𝔹");
        });
    });

    describe("symbols", function () {
        it("should correctly convert `\\alpha` symbol", function () {
            const latex = "\\alpha";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("α");
        });

    });
    it("should not confuse `\\ggg` with `\\gg`", function () {
        const latex = "\\ggg";
        const unicode = convertLaTeXToUnicode(latex);
        expect(unicode).to.equal("⋙");
    });

    it("should being able to convert multiple symbols", function () {
        const latex = "\\alpha * x + \\beta * y = \\gamma";
        const unicode = convertLaTeXToUnicode(latex);
        expect(unicode).to.equal("α * x + β * y = γ");
    });

    it("should being able to convert same symbol multiple times", function () {
        const latex = "\\alpha + \\alpha = 2 * \\alpha";
        const unicode = convertLaTeXToUnicode(latex);
        expect(unicode).to.equal("α + α = 2 * α");
    });

    it("should convert symbols even if their are touching each others", function () {
        const latex = "\\alpha\\beta\\gamma";
        const unicode = convertLaTeXToUnicode(latex);
        expect(unicode).to.equal("αβγ");
    });

    it("should handle 0 args symbols", function () {
        expect(
            convertLaTeX({},
                Object.keys(characterUnicodeChart).sort()
                    .map(cmd => "\\" + cmd)
                    .join("\n")
            )
        ).to.equal(
            "Å\nÆ\nΑ\n⦜\nΒ\n□\n≎\n⋒\nΧ\n∷\n⋓\nÐ\nĐ\nΔ\n◇\nϜ\n⤓\n⇵\n⥐\n⥞\n⥖\n⥟\n⥗\n⇓\n⩖\n⥀\n⥁\n⦆\n⨆\n⨄\n⩓\n⨍\n" +
            "⨇\n⧊\n⩔\n⥂\n⨈\n⨅\n⨯\nɬ\n◒\n◐\n◑\nɷ\n⦙\n⧋\n⎣\n┆\nʤ\nʃ\nɾ\nʔ\nˑ\nʖ\nʌ\nʍ\nː\n˕\n⦠\nɱ\nɲ\n⩟\nɔ\nɤ\nɣ\n" +
            "ʋ\nʊ\n⥄\n˔\n⥇\n‛\nʕ\nɼ\nɖ\nɭ\nɳ\nɽ\nʂ\nʈ\nʐ\nɻ\n◘\n˓\n˒\nə\n◧\n┙\n◨\n◪\n⋥\n⫶\nʧ\n⦀\nɐ\nɥ\nɯ\nɰ\nɹ\nɺ\n" +
            "ɒ\nʇ\nʎ\nˌ\nˈ\n▯\nħ\n℞\nʒ\nΕ\n⩵\nΗ\nℲ\nΓ\nℑ\nΙ\n⋈\nΚ\nϞ\nΛ\n⥡\n⥙\n⥎\n⥚\n⧏\n⥑\n⥠\n⥘\n⥒\n⇐\n⇔\n⇚\n⟸\n" +
            "⟺\n⟹\n↰\nΜ\nŊ\n⪢\n⪡\n≂\n≫\n≎\n≏\n⧏\n≪\n⪢\n⪡\n≾\n⧐\n⊏\n⊐\n≿\nΝ\nŒ\nΩ\nΟ\nΦ\nΠ\nΨ\nℜ\n⥯\nΡ\n⥝\n⥕\n" +
            "⥛\n⧐\n⥏\n⥜\n⥔\n⥓\n⇒\n⥰\n⇛\n↱\n⧴\nϠ\nΣ\nϚ\n⋐\n⋑\nÞ\nΤ\nΘ\n⤒\n⥮\n⇑\n⇕\nΥ\n⊫\n⊩\n‖\n⊪\nΞ\nΖ\nå\næ\nℵ\n" +
            "≌\nα\n⨿\n∠\n≈\n≊\n≆\n♒\n♈\n↜\n↝\n∗\n≍\n¦\n∍\n‵\n∽\n⋍\n⊼\n∵\nβ\nב\n≬\n⋂\n◯\n⋃\n⨀\n⨁\n⨂\n⨆\n★\n▽\n" +
            "△\n⨄\n⋁\n⋀\n◆\n■\n▲\n▼\n◀\n▶\n⊥\n⋈\n⊡\n⊟\n⊞\n⊠\n∙\n≏\n♋\n∩\n♑\n⋅\n⋯\n⋅\n¢\n✓\nχ\n∘\n≗\n↺\n↻\n" +
            "Ⓢ\n⊛\n⊚\n⊝\n⨏\n♣\n∱\n∁\n≅\n∐\n©\n∪\n⋞\n⋟\n⋎\n⋏\n↶\n↷\n†\nד\n⇠\n⇢\n⊣\n⇅\n‡\n⋱\n°\nδ\nð\n╱\n⋄\n♢\n" +
            "Ϝ\n÷\n⋇\nđ\n≐\n≑\n∔\n…\n⩞\n↓\n⇊\n⇃\n⇂\n⋱\n♪\nℓ\n∅\n∊\n≖\n⪖\n⪕\n≡\n≙\nη\nð\n€\n∃\n≒\n♭\n∀\n⊨\n⌢\nγ\n≥\n" +
            "♊\n≥\n≧\n⩾\n≫\n⋙\nℷ\n⪊\n⪈\n≩\n⋧\n≳\n⪆\n⋗\n⋛\n⪌\n≷\n≳\n«\n»\n‹\n›\n≩\nℏ\n♡\n⊹\n∻\n↩\n↪\nℏ\nı\n⊷\n⇒\n" +
            "∈\n∞\n∫\n⊺\nι\n♃\nκ\nλ\n〈\n∾\n{\n⌈\n…\n≤\n↝\n←\n↢\n↽\n↼\n⇇\n↔\n⇆\n⇋\n↭\n⋋\n♌\n≤\n≦\n⩽\n⪅\n⋖\n⋚\n" +
            "⪋\n≲\n≶\n≲\n⌊\n⊲\n♎\n≪\n⌞\n⋘\n⎰\n⪉\n⪇\n≨\n¬\n⋦\n⟵\n⟷\n⟼\n⟶\n↫\n↬\n◊\n⌟\n⋉\n≨\n♂\n✠\n↦\n∡\n☿\n" +
            "℧\n∣\n⊨\n∓\nμ\n⊸\n⇍\n⇎\n⇏\n⊯\n⊮\n∇\n♮\n↗\n¬\n♆\n≠\n∄\nŋ\n⩾\n∋\n↚\n↮\n⩽\n∤\n⁠\n≹\n∉\n≸\n∦\n↛\n⫅\n⋪\n" +
            "⋬\n⋫\n⋭\nν\n⊭\n⊬\n↖\n⊙\nœ\n∮\nω\nο\n⊖\n〚\n〛\n⊕\n⊶\n⊘\n⊗\n∥\n∂\n⊥\n⌆\nϕ\nπ\n¶\n♓\n⋔\n♇\n±\n£\n≺\n≾" +
            "\n≼\n⋨\n⪯\n⪹\n⪵\n≾\n′\n∏\n∝\nψ\n  \n \n♩\n〉\n}\n⌉\n⌕\n®\n⌋\n⊳\nρ\n∟\n⊾\n→\n↣\n⇁\n⇀\n⇄\n⇌\n☾\n⇉\n⇝" +
            "\n⋌\n≓\n⎱\n⋊\n♐\n♄\n♏\n↘\n§\n∖\n♯\n∥\nσ\n∼\n≃\n⌢\n∖\n⌣\n⌣\n \n♠\n∢\n⊓\n⊔\n⨖\n⊏\n⊑\n⊐\n⊒\n□\nß\n⋆\n≛\n⊂\n" +
            "⊆\n⫅\n⊊\n⫋\n≻\n≿\n≽\n⪰\n⪺\n⪶\n⋩\n≿\n∑\n⊃\n⊇\n⫆\n⊋\n⫌\n√\n∯\n↙\nτ\n♉\nϴ\n´\n˘\nˇ\n¨\n`\n¯\n~\n\\\n|" +
            "\n¦\n•\n¢\n©\n¤\n†\n‡\n°\n$\nǂ\n—\n–\n¡\n>\nƕ\n〈\n<\nƞ\n½\n¼\nª\nº\n¶\n˙\n‱\n‰\nɸ\n¿\n“\n”\n'\n〉\n®\n§" +
            "\n£\nθ\n¾\n˜\n×\n™\nʞ\nϑ\n␣\n¥\nþ\n∴\nθ\n≈\n∼\n≋\n×\n⊤\n™\n△\n▽\n◁\n⊴\n≜\n▷\n⊵\n⊧\n↞\n↠\n⌜\n⊴\n⊵\n" +
            "↑\n↕\n↿\n↾\n⊎\nυ\n⋰\n⇈\n♅\n⌝\n⊨\nε\nϰ\n∅\nφ\nϖ\n∝\nϱ\nς\n⊊\n⊋\nϑ\n△\n⊲\n⊳\n⊢\n⋮\n∨\n⊻\n♀\n|\n⋙\n⋘" +
            "\n♍\n∰\n∧\n℘\n≀\nξ\nζ"
        );
    });
    it("should handle special characters", function () {
        expect(
            convertLaTeXToUnicode(
                "\\i\\j\\oe\\OE\\ae\\AE\\aa\\AA\\o\\O\\ss\\l\\L"
            )
        ).to.equal(
            "ıȷœŒæÆåÅøØßłŁ"
        );
    });

    describe("roots", function () {
        it("should convert simple roots to unicode", function () {
            const latex = "\\sqrt[3]{2}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("∛(2)");
        });

        it("should fall back to sup notation for complicated roots", function () {
            const latex = "\\sqrt[15]{100}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("¹⁵√(100)");
        });

        it("should fall back to default square root if degree parameter is omitted", function () {
            const latex = "\\sqrt{100}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("√(100)");
        });

        it("should accept others LaTeX symbols", function () {
            const latex = "\\sqrt{\\pi}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("√(π)");
        });

        it("should correctly convert nested square roots", function () {
            const latex = "\\sqrt{\\sqrt[3]{4}}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("√(∛(4))");
        });
    });

    describe("modifiers", function () {
        it("should being able to apply modifiers on single element", function () {
            const latex = "\\mathbb B";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("𝔹");
        });

        it("should being able to apply modifiers on group of elements", function () {
            const latex = "\\mathbb{BCD}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("𝔹ℂ𝔻");
        });

        // todo what should \frak@ look like?
        // it("should only consume modifier if modified char exists", function () {
        //     const latex = "^5 \\frak@ \\frak k";
        //     const unicode = convertLaTeXToUnicode(latex);
        //     expect(unicode).to.equal("⁵ \\frak{@} 𝔨");
        // });

        it("should being able to apply multiple modifiers", function () {
            const latex = "\\it{IT} and \\bf{bf}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("𝐼𝑇 and 𝐛𝐟");
        });

        it("should being able to apply same modifier multiple times", function () {
            const latex = "\\it{IT} and \\it{it}";
            const unicode = convertLaTeXToUnicode(latex);
            expect(unicode).to.equal("𝐼𝑇 and 𝑖𝑡");
        });

        // todo
        // it("should correctly apply sup modifiers", function () {
        //     const latex = "a^{2} + b^{2} = c^2";
        //     const unicode = convertLaTeXToUnicode(latex);
        //     expect(unicode).to.equal("a² + b² = c²");
        // });
        //
        // it("should being able to apply sub modifiers", function () {
        //     const latex = "u_{3} > u_{2} > u_1";
        //     const unicode = convertLaTeXToUnicode(latex);
        //     expect(unicode).to.equal("u₃ > u₂ > u₁");
        // });

        it("should handle diacritics", function () {
            // todo math mode cmds
            expect(
                convertLaTeX({
                        translateTo: "unicode"
                    },
                    "\\`e" +
                    "\\`u" +
                    "\\`i" +
                    "\\`o" +
                    "\\`a" +
                    "\\`E" +
                    "\\`U" +
                    "\\`I" +
                    "\\`O" +
                    "\\`A" +
                    "\\`c" +
                    "\\` o" + "\\`{aa}" +
                    "\\' o" + "\\'{aa}" +
                    "\\^ o" + "\\^{aa}" +
                    "\\~ o" + "\\~{aa}" +
                    "\\= o" + "\\={aa}" +
                    "\\. o" + "\\.{aa}" +
                    "\\\" o" + "\\\"{aa}" +
                    "\\H o" + "\\H{aa}" +
                    "\\c o" + "\\c{aa}" +
                    "\\k o" + "\\k{aa}" +
                    "\\b o" + "\\b{aa}" +
                    "\\d o" + "\\d{aa}" +
                    "\\r o" + "\\r{aa}" +
                    "\\u o" + "\\u{aa}" +
                    "\\v o" + "\\v{aa}"
                )
            ).to.equal(
                "è" +
                "ù" +
                "ì" +
                "ò" +
                "à" +
                "È" +
                "Ù" +
                "Ì" +
                "Ò" +
                "À" +
                "c\u0300" + "òaàóaáôaâõaãōaāȯaȧöaäőaa̋o̧aa̧ǫaąo̱aa̱ọaạo̊aåŏaăǒaǎ"
            );
        });

        it("should handle cyrillic", function () {
            expect(
                convertLaTeX({
                        translateTo: "unicode"
                    },
                    "\\cyrchar\\CYRF\\cyrchar\\CYRII\\cyrchar\\CYROMEGA\\cyrchar\\CYRG\\cyrchar\\cyrkvcrs\\cyrchar\\cyryo\\cyrchar\\CYRH\\cyrchar\\CYRZHDSC\\cyrchar\\cyrphk\\cyrchar\\CYRTDSC\\cyrchar\\CYRI\\cyrchar\\cyryi\\cyrchar\\CYRDZHE\\cyrchar\\cyriote\\cyrchar\\CYRK\\cyrchar\\CYRSHHA\\cyrchar\\CYRL\\cyrchar\\CYRM\\cyrchar\\CYRCHLDSC\\cyrchar\\CYRNJE\\cyrchar\\CYRYAT\\cyrchar\\CYRA\\cyrchar\\CYRB\\cyrchar\\cyrchrdsc\\cyrchar\\cyrschwa\\cyrchar\\CYRDZE\\cyrchar\\CYRIE\\cyrchar\\CYRC\\cyrchar\\CYRZH\\cyrchar\\CYRD\\cyrchar\\CYRABHCHDSC\\cyrchar\\CYRFITA\\cyrchar\\CYRE\\cyrchar\\CYRABHHA\\cyrchar\\cyrya\\cyrchar\\cyrdzhe\\cyrchar\\CYRIOTLYUS\\cyrchar\\cyrsemisftsn\\cyrchar\\CYRV\\cyrchar\\cyrishrt\\cyrchar\\cyrdje\\cyrchar\\cyrchldsc\\cyrchar\\CYRY\\cyrchar\\cyrndsc\\cyrchar\\CYRZ\\cyrchar\\CYRKHCRS\\cyrchar\\CYRNG\\cyrchar\\CYRCHRDSC\\cyrchar\\CYRYHCRS\\cyrchar\\CYRSHCH\\cyrchar\\CYRUSHRT\\cyrchar\\cyryu\\cyrchar\\cyrksi\\cyrchar\\CYRN\\cyrchar\\CYRO\\cyrchar\\CYRBYUS\\cyrchar\\CYRP\\cyrchar\\CYRZDSC\\cyrchar\\CYRAE\\cyrchar\\CYRR\\cyrchar\\CYRS\\cyrchar\\CYRT\\cyrchar\\CYRABHCH\\cyrchar\\cyruk\\cyrchar\\CYRU\\cyrchar\\cyrii\\cyrchar\\CYRSEMISFTSN\\cyrchar\\cyrghcrs\\cyrchar\\CYRISHRT\\cyrchar\\cyromegatitlo\\cyrchar\\cyrkbeak\\cyrchar\\cyrie\\cyrchar\\cyrzdsc\\cyrchar\\CYRNDSC\\cyrchar\\CYRGUP\\cyrchar\\cyrshch\\cyrchar\\CYRKHK\\cyrchar\\cyrzh\\cyrchar\\CYRJE\\cyrchar\\cyrthousands\\cyrchar\\cyrabhch\\cyrchar\\textnumero\\cyrchar\\cyrng\\cyrchar\\CYRPSI\\cyrchar\\CYRTETSE\\cyrchar\\CYRIOTBYUS\\cyrchar\\cyrnje\\cyrchar\\CYRIOTE\\cyrchar\\cyrdze\\cyrchar\\cyrae\\cyrchar\\CYRHRDSN\\cyrchar\\CYRKOPPA\\cyrchar\\CYRRTICK\\cyrchar\\CYRSCHWA\\cyrchar\\cyrtdsc\\cyrchar\\CYRGHK\\cyrchar\\cyrabhha\\cyrchar\\cyrshha\\cyrchar\\CYRSH\\cyrchar\\cyru\\cyrchar\\cyrkhcrs\\cyrchar\\cyrt\\cyrchar\\CYRERY\\cyrchar\\cyrs\\cyrchar\\cyrr\\cyrchar\\CYROT\\cyrchar\\cyrlyus\\cyrchar\\CYRNHK\\cyrchar\\CYRSFTSN\\cyrchar\\cyrghk\\cyrchar\\cyrp\\cyrchar\\cyrabhdze\\cyrchar\\cyro\\cyrchar\\CYRTSHE\\cyrchar\\cyrn\\cyrchar\\CYRSDSC\\cyrchar\\cyryhcrs\\cyrchar\\cyrpsi\\cyrchar\\cyrz\\cyrchar\\cyry\\cyrchar\\cyrje\\cyrchar\\cyrv\\cyrchar\\cyrchvcrs\\cyrchar\\cyrkhk\\cyrchar\\cyre\\cyrchar\\cyromega\\cyrchar\\cyrd\\cyrchar\\cyrc\\cyrchar\\cyrb\\cyrchar\\CYROTLD\\cyrchar\\cyrgup\\cyrchar\\CYRLJE\\cyrchar\\cyra\\cyrchar\\CYROMEGATITLO\\cyrchar\\CYRGHCRS\\cyrchar\\CYRCHVCRS\\cyrchar\\cyrm\\cyrchar\\cyrl\\cyrchar\\cyrsh\\cyrchar\\cyrk\\cyrchar\\cyri\\cyrchar\\cyrh\\cyrchar\\CYRHDSC\\cyrchar\\CYRIZH\\cyrchar\\CYRABHDZE\\cyrchar\\cyrkdsc\\cyrchar\\cyrg\\cyrchar\\CYRCH\\cyrchar\\cyrf\\cyrchar\\CYRYI\\cyrchar\\cyrmillions\\cyrchar\\CYRKSI\\cyrchar\\CYROMEGARND\\cyrchar\\cyrot\\cyrchar\\cyrtetse\\cyrchar\\cyrhdsc\\cyrchar\\cyrushrt\\cyrchar\\cyriotlyus\\cyrchar\\CYRYA\\cyrchar\\cyrlje\\cyrchar\\cyrotld\\cyrchar\\CYRKDSC\\cyrchar\\cyrhrdsn\\cyrchar\\cyrrtick\\cyrchar\\cyrkoppa\\cyrchar\\CYRDJE\\cyrchar\\cyriotbyus\\cyrchar\\cyrhundredthousands\\cyrchar\\CYRpalochka\\cyrchar\\CYRKVCRS\\cyrchar\\cyromegarnd\\cyrchar\\cyrsftsn\\cyrchar\\cyrabhchdsc\\cyrchar\\cyrzhdsc\\cyrchar\\cyrerev\\cyrchar\\CYRLYUS\\cyrchar\\CYRKBEAK\\cyrchar\\cyrery\\cyrchar\\CYREREV\\cyrchar\\cyrnhk\\cyrchar\\cyrsdsc\\cyrchar\\cyrch\\cyrchar\\cyrtshe\\cyrchar\\CYRPHK\\cyrchar\\CYRYO\\cyrchar\\CYRYU\\cyrchar\\CYRUK"
                    // + "\\cyrchar{\\C:}" // TODO
                )
            ).to.equal(
                "ФІѠГҝёХҖҧҬИїЏѥКҺЛМӋЊѢАБҷәЅЄЦЖДҾѲЕҨяџѨҍВйђӌҮңЗҞҤҶҰЩЎюѯНОѪПҘӔРСТҼѹУіҌғЙѽҡєҙҢҐщӃжЈ҂ҽ№ҥѰҴѬњѤѕӕЪҀҎӘҭҔҩһШуҟтЫсрѾѧӇЬҕпӡоЋнҪұѱзүјвҹӄеѡдцбӨґЉаѼҒҸмлшкихҲѴӠқгЧфЇ҉ѮѺѿҵҳўѩЯљөҚъҏҁЂѭ҈ӀҜѻьҿҗэѦҠыЭӈҫчћҦЁЮѸ"
            );
        });
        it("should handle runes", function () {
            expect(
                convertLaTeX({
                        translateTo: "unicode"
                    },
                    "\\rn{.u}\\rn{y}"
                    // TODO rest
                )
            ).to.equal(
                "ᚤᚤ"
            );
        });
        it("should handle having too little arguments", function () {
            expect(
                convertLaTeX({
                        translateTo: "unicode",
                        onError: (e, i) => `(${e.message})`
                    },
                    "\\binom {arg1}"
                )
            ).to.equal(
                "(Could not find enough arguments for command \\binom. Expected 2, but found 1) arg1"
            );
        });
        it("should handle formatting", function () {
            expect(
                convertLaTeX({
                        translateTo: "unicode"
                    },
                    "\\textsubscript{1}" + "\\subscript{2}" + "\\mathsubscript{3}" +
                    "\\textsuperscript{1}" + "\\superscript{2}" + "\\mathsuperscript{3}" +
                    "\\textbb{a}" + "\\bb{b}" + "\\mathbb{c}" +
                    "\\textbf{a}" + "\\bf{b}" + "\\mathbf{c}" +
                    "\\textfrak{a}" + "\\frak{b}" + "\\mathfrak{c}" +
                    "\\textit{a}" + "\\it{b}" + "\\mathit{c}" +
                    "\\texttt{a}" + "\\tt{b}" + "\\mathtt{c}" +
                    "\\textcal{a}" + "\\cal{b}" + "\\mathcal{c}"
                )
            ).to.equal(
                "₁₂₃¹²³𝕒𝕓𝕔𝐚𝐛𝐜𝔞𝔟𝔠𝑎𝑏𝑐𝚊𝚋𝚌𝓪𝓫𝓬"
            );
        });


    });

    describe("convert", function () {
        it("should correctly convert quadratic formula", function () {
            expect(
                convertLaTeXToUnicode("$\\it x = \\frac{-\\it b \\pm \\sqrt{\\it b^2 - 4\\it{ac}}}{2\\it{a}}$")
            ).to.eq("𝑥 = ((-𝑏 ± √(𝑏² - 4𝑎𝑐)) / (2𝑎))");
        });

        it("should correctly convert Newton binomial formulae", function () {
            expect(
                convertLaTeXToUnicode("$(x + y)^n = \\sum(\\binom{n}{k} * (x^{n-k}y^k))$")
            ).to.eq("(x + y)ⁿ = ∑((n ¦ k) * (xⁿ⁻ᵏyᵏ))");
        });

        it("should correctly convert Euler formulae", function () {
            expect(
                convertLaTeXToUnicode("$\\mathbf{e}^{ix} = cos(x) + \\it{i} sin(x)$")
            ).to.eq("𝐞ⁱˣ = cos(x) + 𝑖 sin(x)");
        });

        it("should correctly convert gravitation formulae", function () {
            expect(
                convertLaTeXToUnicode("$\\vec{F}_{12} = -\\mono G \\centerdot \\frac{\\it m_1\\it m_2}{\\it{d}^2} \\vec{u}_{12}$")
            ).to.eq("F⃗₁₂ = -𝙶 ⋅ ((𝑚₁𝑚₂) / (𝑑²)) u⃗₁₂");
        });

        it("should correctly convert closed path integral", function () {
            expect(
                convertLaTeXToUnicode("\\oint ! \\nabla f \, \\mathbf{d}t = 0")
            ).to.eq("∮ ! ∇ f , 𝐝t = 0");
        });
    });

    describe("fractions", function () {
        it("should convert simple fractions to unicode", function () {
            expect(
                convertLaTeXToUnicode("\\frac{1}{2}")
            ).to.eq(
                "½"
            );
        });

        it("should use fraction slash for numerical fractions", function () {
            expect(
                convertLaTeXToUnicode("\\frac{15}{100}")
            ).to.eq("‌15⁄100‌");
        });

        it("should fall back to conventional notation for complicated fractions", function () {
            expect(
                convertLaTeXToUnicode("\\frac{15a}{100}")
            ).to.eq("((15a) / 100)");
        });

        it("should accept others LaTeX symbols", function () {
            expect(
                convertLaTeXToUnicode("\\frac{\\pi}{3}")
            ).to.eq("(π / 3)");
        });

        it("should correctly convert nested fractions", function () {
            expect(
                convertLaTeXToUnicode("\\frac{\\frac{1}{2}}{\\frac{3}{4}}")
            ).to.eq("(½ / ¾)");
        });
    });
});