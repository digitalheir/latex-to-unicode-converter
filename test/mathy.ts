import {
    unicodeList, UnicodeCharacter,
} from "mathy-unicode-characters";

import {convertLaTeX} from "../src/convert";
import {getAsString} from "../src/unicode/char-util";

import {expect} from "chai";
import {isArray} from "latex-parser";
import {unicode} from "mathy-unicode-characters/ts-compiled/characters/unicode";

import * as fs from "fs";

const startingU = /^U/;

function toArray(x: string | string[]): string[] {
    return isArray(x) ? x as string[] : [x as string];
}

describe("a lot of commands", () => {
    it("should handle all characters", function () {
        const lines = fs.readFileSync("a lot of commands.txt").toString().split("\n");
        lines.forEach(line => {
            // todo
            if (!/^- /.test(line))
                console.log(
                    convertLaTeX({
                        translateTo: "unicode",
                        mode: "Any"
                    }, `\\${line.trim()}`)
                );
        });
    });
});

describe("mathy characters", () => {
    it("should handle all characters", function () {

        unicodeList.sort((a, b) => a._id > b._id ? 1 : a._id < b._id ? -1 : 0).forEach((cmd: UnicodeCharacter) => {
            if (!!cmd.latex) {
                const latexCmd: string = cmd.latex.trim();
                if (![
                        "\\ast",
                        "\\partial",
                        "\\in",

                    ].some(c => c === latexCmd)
                    && latexCmd.match(/^\\[a-zA-Z]+$/)) {
// && (!cmd.description || !(cmd.description.indexOf("COMBINING") >= 0)))
                    const hexaDecimals: number[] = toArray(cmd._id)
                        .map(x => x.replace(startingU, ""))
                        .map((x: string) => parseInt("0x" + x, 16))
                    ;
                    const asStr: string = getAsString(hexaDecimals);

                    try {
                        const src = latexCmd;

                        // unicodeIdentification

                        if (src.indexOf("{") === -1)
                            expect(
                                convertLaTeX({
                                    translateTo: "unicode",
                                    mode: "Any"
                                }, src)
                            ).to.equal(
                                asStr
                            );
                        // }
                    } catch (e) {
                        console.error(
                            "\"" + cmd.latex.replace(/^\\/, "").trim() + "\": \"" + asStr + "\"," + " // " + e.message
                        );
                    }
                }
            }
        });
    });
});
