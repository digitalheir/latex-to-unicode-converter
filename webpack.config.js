const webpack = require("webpack");
const path = require("path");

const libraryName = "latex-to-unicode-converter";

const plugins = [
    new webpack.LoaderOptionsPlugin({
        options: {
            tslint: {
                emitErrors: true,
                failOnHint: true
            }
        }
    })
];

const minifiedFileName = `${libraryName}.min.js`;

console.log(minifiedFileName);

const config = {
    entry: {
        umd: `${__dirname}/src/index.ts`
    },
    devtool: "source-map",
    output: {
        filename: minifiedFileName,
        path: `${__dirname}/`,
        libraryTarget: "umd",
        library: libraryName
    },
    module: {
        rules: [
            {
                enforce: 'pre',
                test: /\.ts$/,
                loader: 'tslint-loader',
                exclude: /node_modules/
            },
            {
                test: /\.ts$/,
                loader: "awesome-typescript-loader",
                options: {
                    configFileName: "tsconfig.webpack.json",
                    useBabel: false
                },
                exclude: /node_modules/
            }
        ]
    },
    resolve: {
        extensions: ['.js', '.ts', '.jsx', '.tsx']
    },

    plugins: plugins
};

module.exports = config;