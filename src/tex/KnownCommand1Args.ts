import {KnownCommand} from "./KnownCommand";

// export interface KnownCommand0OptArgs1Arg extends KnownCommand0OptArgs {
//     argumentCount: 1;
// }
export function createKnownCommandWith1Arg(name: string,): KnownCommand {
    return {
        name,
        optionalArguments: 0,
        argumentCount: 1
    };
}