import {TeXArg} from "latex-parser";

export type CommandCallback = (res: string, err: Error) => any;

export interface KnownCommand {
    name: string;
    optionalArguments: number;
    argumentCount: number;
}

export interface KnownCommand0OptArgs extends KnownCommand {
    optionalArguments: 0;
}

export interface ArgumentNeeded extends KnownCommand {
    apply: (cb: CommandCallback, args: TeXArg[]) => string;
}

export function createCommandHandler(name: string,
                                     optionalArguments: number,
                                     argumentCount: number,
                                     apply: (cb: CommandCallback, args: TeXArg[]) => string) {
    return {name, optionalArguments, argumentCount, apply};
}

export function createKnownCommandWithOptArgs(name: string,
                                              optionalArguments: number,
                                              argumentCount: number): KnownCommand {
    return {name, optionalArguments, argumentCount};
}

export function createKnownCommandWithArgs(name: string,
                                           argumentCount: number): KnownCommand0OptArgs {
    return {
        name,
        optionalArguments: 0,
        argumentCount
    };
}