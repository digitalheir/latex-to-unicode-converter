import {CommandOptions, supportedMarkups} from "./options";
import {LaTeX, latexParser, mustBeOk, mustNotBeUndefined} from "latex-parser";
import {convertLaTeXBlocksToUnicode} from "./unicode/convert";

export function convertLaTeX(options: CommandOptions, src: string): string {
    return convertLaTeXBlocks(options, mustNotBeUndefined(mustBeOk(latexParser.parse(src)).value));
}

export function convertLaTeXToUnicode(src: string): string {
    return convertLaTeXBlocks({
        translateTo: "unicode",
        mode: "Any",
    }, mustNotBeUndefined(mustBeOk(latexParser.parse(src)).value));
}

export function convertLaTeXBlocks(options: CommandOptions,
                                   latex: LaTeX[]): string {
    const {translateTo} = options;

    switch (translateTo) {
        case "html":
            throw new Error("Unsupported format: '"
                + translateTo
                + "'. Use one of: "
                + Object.keys(supportedMarkups)
            );

        case "unicode":
        default:
            return convertLaTeXBlocksToUnicode(options, latex).result;
    }
}

// export function convertString(str: string){
//     parseString()
// }
