export const simpleSuffix = function (modifier: string) {
    return (char: string): string => {
        return char + modifier;
    };
};

export const isSingleTerm = /^.$|^[0-9]+$/;

export function addParenthesis(n: string): string {
    return `(${n})`;
}

// export function stringifyArgs(ars: TeXArg[]): string {
//     return "";// todo ars.map(txts => txts.latex.map(txt => stringifyLaTeXTxt(txt)).join("")).join("")
// }
//
// export function stringifyArg(arg: TeXArg): string {
//     return arg.latex.map(convertLaTeXBlocks)
//     return "iamarg";// todo ars.map(txts => txts.latex.map(txt => stringifyLaTeXTxt(txt)).join("")).join("")
// }