import {LaTeX, TeXArg} from "latex-parser";

export const supportedMarkups = {
    "ascii": true,
    "unicode": true,
    // "md": true,
    "html": true
};

export type SupportedMarkup = keyof typeof supportedMarkups;

export type LaTeXMode = "Text" | "Math" | "Any";

export interface CommandOptions {
    translateTo?: SupportedMarkup;
    mode?: LaTeXMode;
    onError?: (e: Error, l: LaTeX | TeXArg) => string | undefined;
}