import {LaTeX, stringifyLaTeX as stringify, TeXArg} from "latex-parser";

export function stringifyLaTeX(...tex: (LaTeX | TeXArg)[]): string {
    return tex.map(t => stringify(t)).join("");
}