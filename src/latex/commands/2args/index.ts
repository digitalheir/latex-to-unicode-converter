import {fracCmds} from "./frac";

export const twoArgsCommands = Object.assign(
    {},

    fracCmds,
    {
        "binom": true
    });

export function is2argsCommand(name: string): name is keyof typeof twoArgsCommands {
    return twoArgsCommands.hasOwnProperty(name)
        ;
}