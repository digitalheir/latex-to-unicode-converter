export const fracCmds = {
    "frac": true,
    "nfrac": true,
    "cfrac": true,
    "xfrac": true,
    "sfrac": true,
};

export type FracCmd = keyof typeof fracCmds;

export function isFracCmd(x: string): x is FracCmd {
    return fracCmds.hasOwnProperty(x);
}