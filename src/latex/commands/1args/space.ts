export const spaceCmds1arg = {
    kern: true,     // <len> inserts a skip of <len> (may be negative) in text or math mode (a plain TeX skip);
    hskip: true,    // <len> (similar to \kern);
    hspace: true,   // {<len>} inserts a space of length <len> (may be negative) in math or text mode (a LaTeX \hskip);
    hphantom: true, // {<stuff>} inserts space of length equivalent to <stuff> in math or text mode. Should be \protected when used in fragile command (like \caption and sectional headings);
};

export type SpaceCmds1Arg = keyof typeof spaceCmds1arg;