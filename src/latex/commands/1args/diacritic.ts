export const diacriticsTextMode = {
    // Text mode
    //
    "`": true, //{o}	ò	grave accent
    "'": true, // Acute accent
    "^": true, //{o}	ô	circumflex
    "~": true, //{o}	õ	tilde
    "=": true, //{o}	ō	macron accent (a bar over the letter)
    ".": true, //{o}	ȯ	dot over the letter
    '"': true, //{o}	ö	umlaut, trema or dieresis
    "H": true, //{o}	ő	long Hungarian umlaut (double acute)
    "c": true, //{c}	ç	cedilla
    "k": true, //{a}	ą	ogonek
    "b": true, //{o}	o	bar under the letter
    "d": true, //{u}	ụ	dot under the letter\
    "r": true, //{a}	å	ring over the letter (for å there is also the special command \aa)
    "u": true, //{o}	ŏ	breve over the letter
    "v": true, //{s}	š	caron/háček ("v") over the letter
};

/**
 * For example, \breve o → ò
 *
 * todo finish
 */
export const diacriticsMathMode = {
    // Math mode
    //
    "hat": true, // \hat{o}   	    {\displaystyle {\hat {o}}} \hat{o}	circumflex	\^
    "widehat": true, // \widehat{oo}	    {\displaystyle {\widehat {oo}}} \widehat{oo}	wide version of \hat over several letters
    "check": true, // \check{o}     	{\displaystyle {\check {o}}} \check{o}	vee or check	\v
    "tilde": true, // \tilde{o}     	{\displaystyle {\tilde {o}}} \tilde{o}	tilde	\~
    "widetilde": true, // \widetilde{oo}	{\displaystyle {\widetilde {oo}}} \widetilde{oo}	wide version of \tilde over several letters
    "acute": true, // \acute{o}	        {\displaystyle {\acute {o}}} \acute{o}	acute accent	\'
    "grave": true, // \grave{o}	        {\displaystyle {\grave {o}}} \grave{o}	grave accent	\`
    "dot": true, // \dot{o}	        {\displaystyle {\dot {o}}} \dot{o}	dot over the letter	\.
    "ddot": true, // \ddot{o}	        {\displaystyle {\ddot {o}}} \ddot{o}	two dots over the letter (umlaut in text-mode)	\"
    "breve": true, // \breve{o}	        {\displaystyle {\breve {o}}} \breve{o}	breve	\u
    "bar": true, // \bar{o}	        {\displaystyle {\bar {o}}} \bar{o}	macron	\=
    "vec": true, // \vec{o}	        {\displaystyle {\vec {o}}} \vec{o}	vector (arrow) over the letter
    "mathring": true, // \mathring{a}	    å	ring over the letter (for å there is also the special command \aa
};

export type DiacriticCmd1ArgTextMode = keyof typeof diacriticsTextMode;
export type DiacriticCmd1ArgMathMode = keyof typeof diacriticsMathMode;

export type DiacriticCmd1Arg = DiacriticCmd1ArgMathMode | DiacriticCmd1ArgTextMode;