import {SpaceCmds1Arg, spaceCmds1arg} from "./space";
import {
    DiacriticCmd1Arg,
    DiacriticCmd1ArgMathMode,
    DiacriticCmd1ArgTextMode,
    diacriticsMathMode,
    diacriticsTextMode
} from "./diacritic";
import {formattingMath, formattingNoMode, formattingText, isFormattingCmd} from "./formatting";
import {runesMap} from "../../../unicode/commands/1args/symbols/runes";

export type OneArgsCommandName = SpaceCmds1Arg | DiacriticCmd1Arg;

export const oneArgsCommands = Object.assign(
    {},

    spaceCmds1arg,

    formattingText,
    formattingMath,
    formattingNoMode,

    diacriticsTextMode,
    diacriticsMathMode,

    runesMap,
    {
        "cyrchar": true,
        "vec": true,
        "mono": true,
        "ding": true,
        "dingbat": true,
        "ElsevierGlyph": true,
        "elsevierglyph": true,
        "elsevier": true,
        "Elsevier": true,
    }
);

export function is1argsCommand(name: string): name is keyof typeof oneArgsCommands {
    return oneArgsCommands.hasOwnProperty(name);
}
