import {OneArgsCommandName} from "./index";
import {ArgumentNeeded} from "../../../tex/KnownCommand";

export type OneArgsExpander = (name: OneArgsCommandName) => string | ArgumentNeeded | undefined;