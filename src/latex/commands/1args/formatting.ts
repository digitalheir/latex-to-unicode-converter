export const formattingText = {
    textbb: true,
    textbf: true,
    textfrak: true,
    textit: true,
    texttt: true,
    textcal: true,

    textsup: true,
    textsub: true,
    textsuperscript: true,
    textsubscript: true,
};

export const formattingNoMode = {
    bb: true,
    bf: true,
    frak: true,
    it: true,
    tt: true,
    cal: true,

    mono: true,

    sup: true,
    sub: true,
    superscript: true,
    subscript: true,
};

export const formattingMath = {
    mathbb: true,
    mathbf: true,
    mathfrak: true,
    mathit: true,
    mathtt: true,
    mathcal: true,

    mathsup: true,
    mathsub: true,
    mathsuperscript: true,
    mathsubscript: true,
};

export type FormattingCmd =
    keyof typeof formattingMath
    | keyof typeof formattingText
    | keyof typeof formattingNoMode
    ;

export type BbCmd = "bb" | "mathbb" | "textbb";
export function isBbCmd(x: string): x is BbCmd {
    return x === "bb" || x === "mathbb" || x === "textbb";
}
export type BfCmd = "bf" | "mathbf" | "textbf";
export function isBfCmd(x: string): x is BfCmd {
    return x === "bf" || x === "mathbf" || x === "textbf";
}

export type MonoCmd = "mono";
export function isMonoCmd(x: string): x is MonoCmd {
    return x === "mono";
}

export type FrakCmd = "frak" | "mathfrak" | "textfrak";
export function isFrakCmd(x: string): x is FrakCmd {
    return x === "frak" || x === "mathfrak" || x === "textfrak";
}
export type ItCmd = "it" | "mathit" | "textit";
export function isItCmd(x: string): x is ItCmd {
    return x === "it" || x === "mathit" || x === "textit";
}
export type TtCmd = "tt" | "mathtt" | "texttt";
export function isTtCmd(x: string): x is TtCmd {
    return x === "tt" || x === "mathtt" || x === "texttt";
}
export type CalCmd = "cal" | "mathcal" | "textcal";
export function isCalCmd(x: string): x is CalCmd {
    return x === "cal" || x === "mathcal" || x === "textcal";
}

export type SupCmd =
    "sup" | "mathsup" | "textsup"
    | "superscript"
    | "mathsuperscript"
    | "textsuperscript"
    ;
export function isSupCmd(x: string): x is SupCmd {
    return x === "sup"
        || x === "mathsup"
        || x === "textsup"
        || x === "superscript"
        || x === "mathsuperscript"
        || x === "textsuperscript"
        ;
}

export type SubCmd = "sub"
    | "mathsub"
    | "textsub"
    | "subscript"
    | "mathsubscript"
    | "textsubscript"
    ;
export function isSubCmd(x: string): x is SubCmd {
    return x === "sub"
        || x === "mathsub"
        || x === "textsub"
        || x === "subscript"
        || x === "mathsubscript"
        || x === "textsubscript"
        ;
}

export function isFormattingCmd(x: string): x is FormattingCmd {
    return formattingText.hasOwnProperty(x) ||
           formattingMath.hasOwnProperty(x) ||
        formattingNoMode.hasOwnProperty(x);
}