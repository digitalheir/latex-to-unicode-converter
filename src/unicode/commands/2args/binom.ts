import {addParenthesis} from "../../../util";

const isSingleTerm = /^.$|^[0-9]+$/;

// Convert binoms (\binom{a}{b})
export function convertBinom(n: string, d: string): string {
    n = isSingleTerm.test(n) ? n : addParenthesis(n);
    d = isSingleTerm.test(d) ? d : addParenthesis(d);

    return `(${n} ¦ ${d})`;
}
