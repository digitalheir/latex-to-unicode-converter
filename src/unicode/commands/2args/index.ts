import {convertFracToUnicode} from "./frac";
import {convertBinom} from "./binom";

export function expand2argsCommand(name: string, arg1: string, arg2: string): string {
    switch (name) {
        case "frac":
        case "nfrac":
        case "cfrac":
        case "xfrac":
        case "sfrac":
            return convertFracToUnicode(arg1, arg2);
        case "binom":
            return convertBinom(arg1, arg2);
    }
    throw new Error(`No implementation found to expand \\${name} with arguments {${arg1}, ${arg2}`);
}