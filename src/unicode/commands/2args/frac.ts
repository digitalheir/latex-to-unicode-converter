import {addParenthesis, isSingleTerm} from "../../../util";

const zeroWidthNonJoiner = "\u200C";
const regExpDigit = /^[0-9]*$/;


// Convert fractions (\frac{a}{b})
export function convertFracToUnicode(n: string, d: string): string {
    if (n === "1" && d === "2") return "½";
    if (n === "1" && d === "3") return "⅓";
    if (n === "1" && d === "4") return "¼";
    if (n === "1" && d === "5") return "⅕";
    if (n === "1" && d === "6") return "⅙";
    if (n === "1" && d === "8") return "⅛";
    if (n === "2" && d === "3") return "⅔";
    if (n === "2" && d === "5") return "⅖";
    if (n === "3" && d === "4") return "¾";
    if (n === "3" && d === "5") return "⅗";
    if (n === "3" && d === "8") return "⅜";
    if (n === "4" && d === "5") return "⅘";
    if (n === "5" && d === "6") return "⅚";
    if (n === "5" && d === "8") return "⅝";
    if (n === "7" && d === "8") return "⅞";


    if (regExpDigit.test(n) && regExpDigit.test(d)) {
        // exploit font layout engine support for FRACTION SLASH + arabic numerals
        return zeroWidthNonJoiner + n + "⁄" + d + zeroWidthNonJoiner;
    }

    n = isSingleTerm.test(n) ? n : addParenthesis(n);
    d = isSingleTerm.test(d) ? d : addParenthesis(d);
    return "(" + n + " / " + d + ")";

    // let subLeft = str.substring(0, idx);
    // let subRight = str.substring(idx + key.length + num[1] + den[1]);
    // str = subLeft + frac + subRight;
    // return str;
}