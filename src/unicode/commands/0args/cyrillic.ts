import {ZeroArgsExpander} from "./expander";

// NOTE these cmd's by themselves render empty in LaTeX and are properly preceded by \cyrchar. But I guess this is better than how LaTeX handles them.

export const cyrillicUnicodeChart = {
    "CYRF": "Ф",
    "CYRII": "І",
    "CYROMEGA": "Ѡ",
    "CYRG": "Г",
    "cyrkvcrs": "ҝ",
    "cyryo": "ё",
    "CYRH": "Х",
    "CYRZHDSC": "Җ",
    "cyrphk": "ҧ",
    "CYRTDSC": "Ҭ",
    "CYRI": "И",
    "cyryi": "ї",
    "CYRDZHE": "Џ",
    "cyriote": "ѥ",
    "CYRK": "К",
    "CYRSHHA": "Һ",
    "CYRL": "Л",
    "CYRM": "М",
    "CYRCHLDSC": "Ӌ",
    "CYRNJE": "Њ",
    "CYRYAT": "Ѣ",
    "CYRA": "А",
    "CYRB": "Б",
    "cyrchrdsc": "ҷ",
    "cyrschwa": "ә",
    "CYRDZE": "Ѕ",
    "CYRIE": "Є",
    "CYRC": "Ц",
    "CYRZH": "Ж",
    "CYRD": "Д",
    "CYRABHCHDSC": "Ҿ",
    "CYRFITA": "Ѳ",
    "CYRE": "Е",
    "CYRABHHA": "Ҩ",
    "cyrya": "я",
    "cyrdzhe": "џ",
    "CYRIOTLYUS": "Ѩ",
    "cyrsemisftsn": "ҍ",
    "CYRV": "В",
    "cyrishrt": "й",
    "cyrdje": "ђ",
    "cyrchldsc": "ӌ",
    "CYRY": "Ү",
    "cyrndsc": "ң",
    "CYRZ": "З",
    "CYRKHCRS": "Ҟ",
    "CYRNG": "Ҥ",
    "CYRCHRDSC": "Ҷ",
    "CYRYHCRS": "Ұ",
    "CYRSHCH": "Щ",
    "CYRUSHRT": "Ў",
    "cyryu": "ю",
    "cyrksi": "ѯ",
    "CYRN": "Н",
    "CYRO": "О",
    "CYRBYUS": "Ѫ",
    "CYRP": "П",
    "CYRZDSC": "Ҙ",
    "CYRAE": "Ӕ",
    "CYRR": "Р",
    "CYRS": "С",
    "CYRT": "Т",
    "CYRABHCH": "Ҽ",
    "cyruk": "ѹ",
    "CYRU": "У",
    "cyrii": "і",
    "CYRSEMISFTSN": "Ҍ",
    "cyrghcrs": "ғ",
    "CYRISHRT": "Й",
    "cyromegatitlo": "ѽ",
    "cyrkbeak": "ҡ",
    "cyrie": "є",
    "cyrzdsc": "ҙ",
    "CYRNDSC": "Ң",
    "CYRGUP": "Ґ",
    "cyrshch": "щ",
    "CYRKHK": "Ӄ",
    "cyrzh": "ж",
    "CYRJE": "Ј",
    "cyrthousands": "҂",
    "cyrabhch": "ҽ",
    "textnumero": "№",
    "cyrng": "ҥ",
    "CYRPSI": "Ѱ",
    "CYRTETSE": "Ҵ",
    "CYRIOTBYUS": "Ѭ",
    "cyrnje": "њ",
    "CYRIOTE": "Ѥ",
    "cyrdze": "ѕ",
    "cyrae": "ӕ",
    "CYRHRDSN": "Ъ",
    "CYRKOPPA": "Ҁ",
    "CYRRTICK": "Ҏ",
    "CYRSCHWA": "Ә",
    "cyrtdsc": "ҭ",
    "CYRGHK": "Ҕ",
    "cyrabhha": "ҩ",
    "cyrshha": "һ",
    "CYRSH": "Ш",
    "cyru": "у",
    "cyrkhcrs": "ҟ",
    "cyrt": "т",
    "CYRERY": "Ы",
    "cyrs": "с",
    "cyrr": "р",
    "CYROT": "Ѿ",
    "cyrlyus": "ѧ",
    "CYRNHK": "Ӈ",
    "CYRSFTSN": "Ь",
    "cyrghk": "ҕ",
    "cyrp": "п",
    "cyrabhdze": "ӡ",
    "cyro": "о",
    "CYRTSHE": "Ћ",
    "cyrn": "н",
    "CYRSDSC": "Ҫ",
    "cyryhcrs": "ұ",
    "cyrpsi": "ѱ",
    "cyrz": "з",
    "cyry": "ү",
    // TODO
    // "C:": "\u030F",
    "cyrje": "ј",
    "cyrv": "в",
    "cyrchvcrs": "ҹ",
    "cyrkhk": "ӄ",
    "cyre": "е",
    "cyromega": "ѡ",
    "cyrd": "д",
    "cyrc": "ц",
    "cyrb": "б",
    "CYROTLD": "Ө",
    "cyrgup": "ґ",
    "CYRLJE": "Љ",
    "cyra": "а",
    "CYROMEGATITLO": "Ѽ",
    "CYRGHCRS": "Ғ",
    "CYRCHVCRS": "Ҹ",
    "cyrm": "м",
    "cyrl": "л",
    "cyrsh": "ш",
    "cyrk": "к",
    "cyri": "и",
    "cyrh": "х",
    "CYRHDSC": "Ҳ",
    "CYRIZH": "Ѵ",
    "CYRABHDZE": "Ӡ",
    "cyrkdsc": "қ",
    "cyrg": "г",
    "CYRCH": "Ч",
    "cyrf": "ф",
    "CYRYI": "Ї",
    "cyrmillions": "\u0489",
    "CYRKSI": "Ѯ",
    "CYROMEGARND": "Ѻ",
    "cyrot": "ѿ",
    "cyrtetse": "ҵ",
    "cyrhdsc": "ҳ",
    "cyrushrt": "ў",
    "cyriotlyus": "ѩ",
    "CYRYA": "Я",
    "cyrlje": "љ",
    "cyrotld": "ө",
    "CYRKDSC": "Қ",
    "cyrhrdsn": "ъ",
    "cyrrtick": "ҏ",
    "cyrkoppa": "ҁ",
    "CYRDJE": "Ђ",
    "cyriotbyus": "ѭ",
    "cyrhundredthousands": "\u0488",
    "CYRpalochka": "Ӏ",
    "CYRKVCRS": "Ҝ",
    "cyromegarnd": "ѻ",
    "cyrsftsn": "ь",
    "cyrabhchdsc": "ҿ",
    "cyrzhdsc": "җ",
    "cyrerev": "э",
    "CYRLYUS": "Ѧ",
    "CYRKBEAK": "Ҡ",
    "cyrery": "ы",
    "CYREREV": "Э",
    "cyrnhk": "ӈ",
    "cyrsdsc": "ҫ",
    "cyrch": "ч",
    "cyrtshe": "ћ",
    "CYRPHK": "Ҧ",
    "CYRYO": "Ё",
    "CYRYU": "Ю",
    "CYRUK": "Ѹ",
};

export const cyrillicUnicode: ZeroArgsExpander = (name) => cyrillicUnicodeChart[<keyof typeof cyrillicUnicodeChart>name];