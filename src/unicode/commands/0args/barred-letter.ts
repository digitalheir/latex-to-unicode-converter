
import {ZeroArgsExpander} from "./expander";

//{}	ł	barred l (l with stroke)
export const barredLUnicodeChart = {
    l: "ł",
    L: "Ł"
};

export const barredLUnicode: ZeroArgsExpander = (name) => barredLUnicodeChart[<keyof typeof barredLUnicodeChart>name];