// "o": slashed //	ø	slashed o (o with stroke)

import {ZeroArgsExpander} from "./expander";

export const slashed_o = "ø";
export const slashed_O = "Ø";

export const slashedOUnicodeChart = {
    "o": slashed_o,
    "O": slashed_O
};

export const slashedOUnicode: ZeroArgsExpander = (char) => slashedOUnicodeChart[<keyof typeof slashedOUnicodeChart>char];