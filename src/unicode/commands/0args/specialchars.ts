import {ZeroArgsExpander} from "./expander";
export const specialCharacters = {
    "i": "ı",
    "j": "ȷ",
    "oe": "œ",
    "OE": "Œ",
    "ae": "æ",
    "AE": "Æ",
    "aa": "å",
    "AA": "Å",
    "o": "ø",
    "O": "Ø",
    "ss": "ß",
    "l": "ł",
    "L": "Ł"
};


export type SpecialCharacter = keyof typeof specialCharacters;

export function isSpecialCharacter(x: string): x is SpecialCharacter {
    return specialCharacters.hasOwnProperty(x);
}

export const specialCharacter: ZeroArgsExpander = (name) => isSpecialCharacter(name) ? specialCharacters[name] : undefined;
