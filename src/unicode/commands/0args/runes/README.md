# Tables mapping runic characters between Unicode and allrunes LaTeX commands

Based on Carl-Gustav Werner's excellent work [*allrunes*](https://www.ctan.org/pkg/allrunes).

## The Runic Word Separator Symbols

| Codepoint | Rune      | Name        |  LaTeX        |
| --------- | --------- | ----------- |  ------------ |
| 16EB      | ᛫ | RUNIC SINGLE PUNCTUATION |  \dot         |
| 16EC      | ᛬ | RUNIC MULTIPLE PUNCTUATION |  \doubledot   |
| 205D      | ⁝ | TRICOLON                 |  \tripledot   |
| 205E      | ⁞ | VERTICAL FOUR DOTS       |  \quaddot     |
| 2E58      | ⹘ | VERTICAL FIVE DOTS ([proposal](http://unicode.org/L2/L2015/15327-n4704-medieval-punct.pdf)) |  \pentdot     | 
| 2E3D      | ⸽ | VERTICAL SIX DOTS        |               |
| 16EB      | ᛫ | RUNIC SINGLE PUNCTUATION |  \eye         |
| 16EC      | ᛬ | RUNIC MULTIPLE PUNCTUATION |  \doubleeye   |
| 22EE      | ⋮ | VERTICAL ELLIPSIS        |  \tripleeye   |
| 205E      | ⁞ | VERTICAL FOUR DOTS       |  \quadeye     |
| 2E58      | ⹘ | VERTICAL FIVE DOTS ([proposal](http://unicode.org/L2/L2015/15327-n4704-medieval-punct.pdf)) |  \penteye     |
| 2E3D      | ⸽ | VERTICAL SIX DOTS        |               |
| 2758      | ❘ | LIGHT VERTICAL BAR (~)   |  \bar         |
| 00A6      | ¦ | BROKEN BAR              |  \doublebar   |
|           |   |                         |  \triplebar   |
| 16ED      | ᛭ | RUNIC CROSS PUNCTUATION |  \plus        |
| 2021      | ‡ | DOUBLE DAGGER (~)       | \doubleplus  |
|           |   |                         |  \tripleplus  |
| 2A2F      | ⨯ | VECTOR OR CROSS PRODUCT (~) |  \cross       |
|           |   |                         |  \doublecross | 
|           |   |                         |  \triplecross |
| 002A      | * | ASTERISK                |  \star        |

 ## The Common Germanic Runes

### Main runes

| Codepoint |Rune      | Name                     | Transliteration  | LaTeX       |
|-----------|----------|--------------------------|------------------|------------- |
| 16A0      | ᚠ        | FEHU FEOH FE F           | **f**            | f            |
| 16A2      | ᚢ        | URUZ UR U                | **u**            | u            |
| 16A6      | ᚦ        | THURISAZ THURS THORN     | **þ**            | \th         |
| 16A8      | ᚨ        | ANSUZ A                  | **a**            | a           |
| 16B1      | ᚱ        | RAIDO RAD REID R         | **r**            | r           |
| 16B2      | ᚲ        | KAUNA                    | **k**            | k           |
| 16B7      | ᚷ        | GEBO GYFU G              | **g**            | g           |
| 16B9      | ᚹ        | WUNJO WYNN W             | **w**            | w           |
| 16BA      | ᚺ        | HAGLAZ H                 | **h**            | h           |
| 16BB      | ᚻ        | HAEGL H                  | **h**            | \h          |
| 16BE      | ᚾ        | NAUDIZ NYD NAUD N        | **n**            | n           |
| 16C1      | ᛁ        | ISAZ IS ISS I            | **i**            | i           |
| 16C3      | ᛃ        | JERAN J                  | **j**            | j           |
| 16C7      | ᛇ        | IWAZ EOH                 | **ï** or **ẹ**   | I or ï      |
| 16C8      | ᛈ        | PERTHO PEORTH P          | **p**            | p           |
| 16C9      | ᛉ        | ALGIZ EOLHX              | **z** or **R**   | R           |
| 16CA      | ᛊ        | SOWILO S                 | **s**            | s or \sfour |
| 16CF      | ᛏ        | TIWAZ TIR TYR T          | **t**            | t           |
| 16D2      | ᛒ        | BERKANAN BEORC BJARKAN B | **b**            | b           |
| 16D6      | ᛖ        | EHWAZ EH E               | **e**            | e           |
| 16D7      | ᛗ        | MANNAZ MAN M             | **m**            | m           |
| 16DA      | ᛚ        | LAUKAZ LAGU LOGR L       | **l**            | l           |
| 16DC      | ᛜ        | INGWAZ                   | **ŋ**            | \ng         |
| 16DE      | ᛞ        | DAGAZ DAEG D             | d                | d           |
| 16DF      | ᛟ        | OTHALAN ETHEL O          | o                | o           |

### Variants

| Main rune | Codepoint | Variant rune  | Name | Transliteration | LaTeX    |
|-----------|-----------|---------------|------|-----------------|----------|
| ᚠ         |           |               |      | **f**           | F |
| ᚢ         |           |               |      | **u**           | U  | 
| ᚲ         |           |               |      | **k**           | \k |
| ᚲ         |           |               |      | **k**           | K |
| ᚲ         |           |               |      | **k**           | \K |
| ᚺ         |           |               |      | **h**           | H |
| ᛃ         |           |               |      | **j**           | \j |
| ᛃ         |           |               |      | **j**           | J |
| ᛃ         |           |               |      | **A**           | A |
| ᛃ         |           |               |      | **A**           | \A |
| ᛈ         |           |               |      | **p**           | \p |
| ᛈ         |           |               |      | **p**           | P |
| ᛉ         |           |               |      | **z** or **R**  | \R  |
| ᛉ         |           |               |      | **z** or **R**  | \RR |
| ᛊ         |           |               |      | **s**           | S or \ssix |
| ᛊ         |           |               |      | **s**           | \s |
| ᛊ         |           |               |      | **s**           | \S |
| ᛊ         |           |               |      | **s**           | \sthree |
| ᛊ         |           |               |      | **s**           | \sfive |
| ᛊ         |           |               |      | **s**           | \sseven |
| ᛊ         |           |               |      | **s**           | \seight |
| ᛏ         |           |               |      | **T**           | t    | T |
| ᛒ         |           |               |      | **B**           | b    | B |
| ᛖ         |           |               |      | **E**           | e    | E |
| ᛜ         |           |               |      |  **ŋ**           | \NG |
| ᛜ         |           |               |      |  **ŋ** or **iŋ** | \ing |
| ᛜ         |           |               |      |  **ŋ** or **iŋ** | \Ing |
| ᛜ         |           |               |      |  **ŋ** or **iŋ** | \ING |
| ᛞ         |           |               |      | **d**           | \d |
| ᛞ         |           |               |      | **d**           | D  |
|           |           |               |      | **i**           | \i   |
|           |           |               |      | **a**           | \a   |

 ## The Anglo-Frisian Runes
 
|  Rune & Translit. & \LaTeX & Rune & Translit. & \LaTeX \\
|
|  \\hline
|  \ra{f}   & f   & f   & \ra{F}      & f   & F\\
|  \ra{u}   & u   & u   & \ra{U}      & u   & U\\
|  \ra{\th} & \ts{\th} & \th or \texttt{\th} &             &          & \\
|  \ra{o}   & o   & o   &             &          & \\
|  \ra{r}   & r   & r   &             &          & \\
|  \ra{c}   & c   & c   & \ra{\c}     & c   & \c\\
|  \ra{g}   & g   & g   &             &          & \\
|  \ra{w}   & w   & w   &             &          & \\
|  \ra{h}   & h   & h   & \ra{\h}     & h   & \h  \\
|           &          &            & \ra{H}      & h   & H\\
|  \ra{n}   & n   & n   &             &          & \\
|  \ra{i}   & i   & i   &             &          & \\
|  \ra{j}   & j   & j   & \ra{\j}     & j   & \j\\
|           &          &            & \ra{J}      & j   & J\\
|  \ra{I}   & \ts{\sharpi} & I or \texttt{\"i}
|                                   &             &          & \\
|  \ra{p}   & p   & p   & \ra{P}      & p(?)& P\\
|  \ra{x}   & x   & x   &             &          & \\
|  \ra{s}   & s   & s   & \ra{S}      & s   & S\\
|           &          &            & \ra{\sthree} & s  & \sthree\\
|           &          &            & \ra{\sfour} & s   & \sfour\\
|           &          &            & \ra{\sfive} & s   & \sfive\\
|           &          &            & \ra{\ssix}  & s   & \ssix\\
|           &          &            & \ra{\sseven} & s   & \sseven\\
|           &          &            & \ra{\seight} & s   & \seight\\
|  \ra{t}   & t   & t   &             &          & \\
|  \ra{b}   & b   & b   & \ra{B}      & b   & B\\
|  \ra{e}   & e   & e   &             &          & \\
|  \ra{m}   & m   & m   &             &          & \\
|  \ra{l}   & l   & l   &             &          & \\
|  \ra{\ng} & \ts{\ng} & \ng  or \texttt{\ng}
|                                    &            &          & \\
|  \ra{d}   & d   & d   & \ra{\d}      & d   & \d\\
|           &          &            & \ra{D}     & d   & D\\
|  \ra{\oe} & \ts{\oe} & \oe or \texttt{\oe}  & \ra{\OE} & \ts{\oe} & \OE or \texttt{\OE} \\
|  \ra{a}   & a   & a   &             &          & \\
|  \ra{\ae} & \ts{\ae} & \ae or \texttt{\ae}  &             &          & \\
|  \ra{y}   & y   & y   & \ra{\y}     & y   & \y\\
|           &          &            & \ra{Y}      & y   & Y\\
|  \ra{\ea} & \ts{\t ea} & \ea &             &          & \\
|  \ra{\g} & \ts{\=g} & \g & \ra{\G}    & \ts{\=g} & \G \\
|  \ra{k}   & \ts{k}   & k   &             &          & \\
|  \ra{\k} & \ts{\=k} & \k &             &          & \\
|  \ra{\rex} & \ts{rex}(?)   & \rex   &             &          & \\
|  \\hline
|  \ra{q}   & \ts{q}   & q   &             &          & \\
|  \ra{\stan} & \ts{st}(?)   & \stan   & \ra{\STAN} & \ts{st}(?)   & \STAN   \\

 ## The Normal Runes
 \begin{tabular}{|c|c|c|c|c|c|}
   \\hline
   \multicolumn{3}{|c|}{}                                            &
   \multicolumn{3}{c|} {\raisebox{-1pt}[0cm][0cm]{Dotted runes and}} \\
   \multicolumn{3}{|c|}{\raisebox{ 2ex}[0cm][0cm]{Main runes}}       &
   \multicolumn{3}{c|} {\raisebox{ 1pt}[0cm][0cm]{variant forms}}    \\
   \\hline
   Rune & Translit. & \LaTeX & Rune & Translit. & \LaTeX \\
   \\hline
   \rn{f}   & \ts{f}     & f   & \rn{F}    & \ts{f}     & F\\
   \rn{u}   & \ts{u}     & u   & \rn{y}    & \ts{y}     & \.u or y \\
   \rn{\th} & \ts{\th}   & \th or \texttt{\th}
                                      & \rn{\TH}  & \ts{\th}   & \TH or \texttt{\TH}\\
   \rn{A}   & \ts{\k{a}} & A   & \rn{\A}   & \ts{\k{a}} & \A\\
   \rn{r}   & \ts{r}     & r   &           &            & \\
   \rn{k}   & \ts{k}     & k   & \rn{g}    & \ts{g}     & \.k or g \\
   \rn{h}   & \ts{h}     & h   &           &            & \\
   \rn{n}   & \ts{n}     & n   &           &            & \\
   \rn{i}   & \ts{i}     & i   & \rn{e}    & \ts{e}     & \.i or e \\
   \rn{a}   & \ts{a}     & a   &           &            & \\
   \rn{s}   & \ts{s}     & s   & \rn{S}    & \ts{s}     & S\\
   \rn{t}   & \ts{t}     & t   &           &            & \\
   \rn{b}   & \ts{b}     & b   & \rn{B}    & \ts{b}     & B\\
   \rn{m}   & \ts{m}     & m   & \rn{\"m}  & \ts{m}     & \"m or \m\\
            &            &            & \rn{M}    & \ts{m}     & M\\
            &            &            & \rn{\"M}  & \ts{m}     & \"M or \M\\
   \rn{l}   & \ts{l}     & l   &           &            & \\
   \rn{R}   & \ts{R}     & R   &           &            & \\
 \\hline
 \end{tabular}\\

 ## The Short-Twig Runes
 \noindent
 \begin{tabular}{|c|c|c|c|c|c|}
   \\hline
   \multicolumn{3}{|c|}{Main runes} & \multicolumn{3}{c|} {Variant forms}    \\
   \\hline
   Rune & Translit. & \LaTeX & Rune & Translit. & \LaTeX \\
   \\hline
   \rt{f}   & \ts{f}     & f   & \rt{F}    & \ts{f}     & F\\
   \rt{u}   & \ts{u}     & u   & \rt{U}    & \ts{u}     & U \\
   \rt{\th} & \ts{\th}   & \th or \texttt{\th}
                                      & \rt{\TH}  & \ts{\th}   & \TH or \texttt{\TH}\\
   \rt{A}   & \ts{\k{a}} & A   & \rt{\A}   & \ts{\k{a}} & \A\\
   \rt{r}   & \ts{r}     & r   &           &            & \\
   \rt{k}   & \ts{k}     & k   &           &            & \\
   \rt{h}   & \ts{h}     & h   & \rt{\h}   & \ts{h}     & \h\\
   \rt{n}   & \ts{n}     & n   &           &            & \\
   \rt{i}   & \ts{i}     & i   &           &            & \\
   \rt{a}   & \ts{a}     & a   &           &            & \\
   \rt{s}   & \ts{s}     & s   & \rt{\s}   & \ts{s}     & \s\\
   \rt{t}   & \ts{t}     & t   & \rt{\t}   & \ts{t}     & \t\\
            &            &            & \rt{T}    & \ts{t}     & T \\
   \rt{b}   & \ts{b}     & b   & \rt{\b}   & \ts{b}     & \b \\
   \rt{m}   & \ts{m}     & m   & \rt{\m}   & \ts{m}     & \m\\
            &            &            & \rt{M}    & \ts{m}     & M\\
   \rt{l}   & \ts{l}     & l   & \rt{\l}   & \ts{l}     & \l\\
            &            &            & \rt{L}    & \ts{l}     & L \\
   \rt{R}   & \ts{R}     & R   & \rt{\R}   & \ts{R}     & \R\\
 \\hline
 \end{tabular}\\

 \noindent
 The runic characters are compiled from
 the following sources: \cite{Enoksen}, \cite{Jansson} and \cite{Moltke}.

 \pagebreak

 ## The Staveless Runes
 \noindent
 \begin{tabular}{|c|c|c|c|c|c|}
   \\hline
   \multicolumn{3}{|c|}{Main runes} & \multicolumn{3}{c|} {Variant forms}    \\
   \\hline
   Rune & Translit. & \LaTeX & Rune & Translit. & \LaTeX \\
   \\hline
   \rl{f}   & \ts{f}     & f & & & \\
   \rl{u}   & \ts{u}     & u & & &  \\
   \rl{\th} & \ts{\th}   & \th or \texttt{\th} & & &  \\
   \rl{A}   & \ts{\k{a}} & A &  \rl{\A}   & \ts{\k{a}} & \A \\
   \rl{r}   & \ts{r}     & r & & &  \\
   \rl{k}   & \ts{k}     & k & & &  \\
   \rl{h}   & \ts{h}     & h & & &  \\
   \rl{n}   & \ts{n}     & n & & &  \\
   \rl{i}   & \ts{i}     & i & & &  \\
   \rl{a}   & \ts{a}     & a & & &  \\
   \rl{s}   & \ts{s}     & s & & &  \\
   \rl{t}   & \ts{t}     & t & & &  \\
   \rl{b}   & \ts{b}     & b & & &  \\
   \rl{m}   & \ts{m}     & m & & &  \\
   \rl{l}   & \ts{l}     & l & & &  \\
   \rl{R}   & \ts{R}     & R & & &  \\
 \\hline
 \end{tabular}\\

 \noindent The main source for the runic characters is \cite{Peterson94}.

 This font covers the staveless runes known mainly from H\"alsingland, Sweden. Other
 staveless runes occurred in S\"odermanland, Sweden; they are not covered here.

 The rune for \ts{\k{a}} has never been found
 in a typical inscription. Its assumed appearance, \rl{A},
 is based upon the symmetry considerations, and is a rather widely accepted guess.
 However, in a late Norwegian inscription, the form \rl{\A} was used, p.~244 in~\cite{Peterson94}.

 The height of the \rl{f}, \rl{k}, \rl{h} and \rl{s} runes could vary quite a lot;
 here only some kind of average form is used.

 \pagebreak

 ## The Medieval Runes
 \begin{tabular}{|c|c|c|c|c|c|}
   \\hline
   \multicolumn{3}{|c|}{Main runes} & \multicolumn{3}{c|} {Variant forms}    \\
   \\hline
   Rune & Translit. & \LaTeX & Rune & Translit. & \LaTeX \\
   \\hline
   \rm{a}   & \ts{a}     & a   & \rm{\.a}  & \ts{a}     & \a or \.a \\
            &            &            & \rm{\'a}  & \ts{a} (or \ts{\r a})    & \adot or \'a\\
            &            &            & \rm{A}    & \ts{a}     & A \\
   \rm{b}   & \ts{b}     & b   & \rm{B}    & \ts{b}     & B \\
   \rm{c}   & \ts{c}     & c   & \rm{\.c}  & \ts{c}     & \c or \.c \\
            &            &            & \rm{\^c}  & \ts{c}     & C or \^c \\
   \rm{d}   & \ts{d}     & d or \.t &
   \rm{\=t} & \ts{d}     & \tbar or \=t\\
            &            &            & \rm{\'t}  & \ts{d}     & \tdot or \'t\\
            &            &            & \rm{\~t}  & \ts{d}     & \tflag or \~t\\
            &            &            & \rm{\^t}  & \ts{d}     & \tring or \^t\\
            &            &            & \rm{D}    & \ts{d}     & D or \.T \\
            &            &            & \rm{\"T}  & \ts{d}     & \D or \"T\\
   \rm{e}   & \ts{e}     & e or \.i &
   \rm{\e}  & \ts{e}     & \e or \=i \\
            &            &            & \rm{\^i}  & \ts{e}     & E or \^i\\
   \rm{f}   & \ts{f}     & f   & \rm{F}    & \ts{f}     & F \\
   \rm{g}   & \ts{g}     & g or \.k &
   \rm{G}   & \ts{g}     & G or \'k \\
            &            &            & \rm{\=k}  & \ts{g}     & \g or \=k\\
   \rm{h}   & \ts{h}     & h   & \rm{\h}   & \ts{h}     & \h \\
            &            &            & \rm{H}    & \ts{h}     & H \\
   \rm{i}   & \ts{i}     & i   &           &            & \\
   \rm{k}   & \ts{k}     & k   &           &            & \\
   \rm{l}   & \ts{l}     & l   & \rm{\.l}  & \ts{l}     & \l or \.l\\
            &            &            & \rm{\=l}  & \ts{l}     & \lbar or \=l\\
            &            &            & \rm{\'l}  & \ts{l}     & \ldot or \'l\\
            &            &            & \rm{\~l}  & \ts{l}     & \lflag or \~l\\
            &            &            & \rm{\^l}  & \ts{l}     & \lring or \^l\\
   \rm{m}   & \ts{m}     & m   & \rm{\m}   & \ts{m}     & \m \\
   \rm{n}   & \ts{n}     & n   & \rm{\.n}  & \ts{n}     & \n or \.n\\
            &            &            & \rm{\'n}  & \ts{n}     & \ndot or \'n \\
            &            &            & \rm{N}    & \ts{n}     & N \\
            &            &            & \rm{\.N}  & \ts{n}     & \N or \.N\\
   \rm{o}   & \ts{o}     & o   &           &            & \\
   \rm{p}   & \ts{p}     & p or \"b
                                      & \rm{\.b}  & \ts{p}     & \p or \.b \\
            &            &            & \rm{\P}   & \ts{p}     & \P \\
            &            &            & \rm{\"\P} & \ts{p}     & \Pdots or \"\P \\
            &            &            & \rm{P}    & \ts{p}     & P \\
   \rm{q}   & \ts{q}     & q   & \rm{\q}   & \ts{q}     & \q \\
            &            &            & \rm{\Q}   & \ts{q}     & \Q \\
   \rm{r}   & \ts{r}     & r   & \rm{\.r}  & \ts{\"r}   & \rdot or \.r\\
            &            &            & \rm{\r}   & \ts{r}     & \r \\
   \\hline
 \end{tabular}
 \pagebreak

