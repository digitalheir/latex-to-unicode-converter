// export const runeSeparators = {
// % Here are the declaration of the runic word separators.
// % Of course they should not be here! They should reside in the fd-files.
// % And they do. Here only a command for defining them are defined, which is then
// % used in the fd-files. Silly? Ugly? But it works.
//     %
//     % Some of them have more than one name to make things orthogonal.
// % All definitions occurres in all encodings.
// %    \begin{macrocode}
// \newcommand{\DeclareRuneSeparators}[1]{%
// \DeclareTextSymbol{\dot}{#1}{46}        % .
// \DeclareTextSymbol{\doubledot}{#1}{58}  % :
// \DeclareTextSymbol{\tripledot}{#1}{59}
//   \DeclareTextSymbol{\quaddot}{#1}{60}
//   \DeclareTextSymbol{\pentdot}{#1}{61}
//
//   \DeclareTextSymbol{\eye}{#1}{46}        % .
// \DeclareTextSymbol{\doubleeye}{#1}{58}  % :
// \DeclareTextSymbol{\tripleeye}{#1}{62}
//   \DeclareTextSymbol{\quadeye}{#1}{63}
//   \DeclareTextSymbol{\penteye}{#1}{64}
//
//   \DeclareTextSymbol{\bar}{#1}{33}        % !
//   \DeclareTextSymbol{\doublebar}{#1}{34}
//   \DeclareTextSymbol{\triplebar}{#1}{35}
//
//   \DeclareTextSymbol{\cross}{#1}{42}      % *
// \DeclareTextSymbol{\doublecross}{#1}{37}
//   \DeclareTextSymbol{\triplecross}{#1}{38}
//
//   \DeclareTextSymbol{\plus}{#1}{43}       % +
//   \DeclareTextSymbol{\doubleplus}{#1}{44}
//   \DeclareTextSymbol{\tripleplus}{#1}{45}
//
//   \DeclareTextSymbol{\star}{#1}{36}
// %\DeclareTextSymbol{\doublestar}{#1}{37}
// %\DeclareTextSymbol{\triplestar}{#1}{}
//
// } % end of newcommand{\DeclareRuneSeparators}
// };

// export const runeSeparatorUnicode = (name) => cyrillicUnicodeChart[<keyof typeof cyrillicUnicodeChart>name];