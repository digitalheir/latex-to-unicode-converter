import {spaceUnicode} from "./space";
import {characterUnicode} from "./symbols";
import {barredLUnicode} from "./barred-letter";
import {slashedOUnicode} from "./slashed";
import {ZeroArgsExpander} from "./expander";
import {cyrillicUnicode} from "./cyrillic";
import {specialCharacter} from "./specialchars";

export const expand0argsCommand: ZeroArgsExpander = (name) => {
    for (const fn of [
        barredLUnicode
        , spaceUnicode
        , slashedOUnicode
        , characterUnicode
        , specialCharacter
        , cyrillicUnicode
    ]) {
        const result = fn(name);
        if (!!result) return result;
    }
};