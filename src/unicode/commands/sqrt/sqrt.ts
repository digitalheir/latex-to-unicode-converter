import {translateCharToSuperscript} from "../1args/formatting/superscript";

function determineSqrtSymbol(base?: string): string {
    const trimmd = base ? base.trim() : undefined;
    if (!trimmd)
        return "√";
    switch (trimmd) {
        case "2":
            return "√";
        case "3":
            return "∛";
        case "4":
            return "∜";
        default:
            const chars: string[] = [];
            for (let i = 0; i < trimmd.length; i++) {
                const char = translateCharToSuperscript(trimmd.charAt(i));
                if (!char) throw new Error("Could not translate \"" + char + "\" to superscript");
                chars.push(char);
            }
            return chars.join("") + "√";
    }
}

export function convertSqrtToUnicode(nucleus: string, base?: string): string {
    const sqrt = determineSqrtSymbol(base);
    const trimmedNucleus = nucleus.trim();
    if (trimmedNucleus === "") {
        return sqrt;
    } else {
        return `${sqrt}(${trimmedNucleus})`;
    }
}

