import {translateCharToBlackboard} from "./blackboard";
import {translateCharToBold} from "./boldfont";
import {translateCharToFraktur} from "./fraktur";
import {translateCharToItalic} from "./italic";
import {translateCharToMonospace} from "./monospace";
import {translateCharToCalligraphic} from "./textcal";
import {
    isBbCmd,
    isBfCmd, isCalCmd, isFrakCmd, isItCmd, isSubCmd,
    isSupCmd,
    isMonoCmd,
    isTtCmd
} from "../../../../latex/commands/1args/formatting";
import {ZeroArgsExpander} from "../../0args/expander";
import {translateCharToSubscript} from "./subscript";
import {translateCharToSuperscript} from "./superscript";
import {translateCharToMono} from "./mono";

export const formattingUnicode = (cmdName: string, arg: string): string | undefined => {
    let fn: ZeroArgsExpander | undefined = undefined;

    if (isBbCmd(cmdName))
        fn = translateCharToBlackboard;
    else if (isBfCmd(cmdName))
        fn = translateCharToBold;
    else if (isFrakCmd(cmdName))
        fn = translateCharToFraktur;
    else if (isItCmd(cmdName))
        fn = translateCharToItalic;
    else if (isTtCmd(cmdName))
        fn = translateCharToMonospace;
    else if (isCalCmd(cmdName))
        fn = translateCharToCalligraphic;
    else if (isSubCmd(cmdName))
        fn = translateCharToSubscript;
    else if (isSupCmd(cmdName))
        fn = translateCharToSuperscript;
    else if (isMonoCmd(cmdName))
        fn = translateCharToMono;

    if (!!fn) {
        const fun: ZeroArgsExpander = fn;
        return arg.split("").map(char => (fun(char) || char)).join("");
    } else
        return undefined;
};