import {diacriticUnicode} from "./diacritics/index";
import {formattingUnicode} from "./formatting/index";
import {translateCharToCyrillic} from "./letters/cyrillic";
import {translateCharToDingbat} from "./symbols/dingbats";
import {translateCharToElsevier} from "./symbols/elsevier";
import {runeUnicode} from "./symbols/runes";

export function expand1argsCommand(name: string, arg: string): string {
    switch (name) {
        case "cyrchar":
            const c = translateCharToCyrillic(arg);
            if (!!c) return c;
            break;

        case "ding":

        case "dingbat":
            const d = translateCharToDingbat(arg);
            if (!!d) return d;
            break;

        case "ElsevierGlyph":

        case "elsevierglyph":
        case "elsevier":
        case "Elsevier":
            const e = translateCharToElsevier(arg);
            if (!!e) return e;
            break;
        default:
            for (const fn of [
                diacriticUnicode,
                formattingUnicode,
                runeUnicode,
            ]) {
                const result = fn(name, arg);
                if (!!result)
                    return result;
            }
    }

    throw new Error("No implementation found to expand \\" + name + " with argument {" + arg + "}");
}
