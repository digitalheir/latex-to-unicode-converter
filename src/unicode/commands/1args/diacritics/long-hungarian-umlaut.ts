import {lookupOrAppend} from "../../command-expander";

// https://en.wikipedia.org/wiki/Double_acute_accent

export const longHungarianUmlaut = lookupOrAppend({
    o: "ő",
    u: "ű",

    O: "Ő",
    U: "Ű",

    y: "Ӳ",
    Y: "ӳ"
}, "\u030B");
