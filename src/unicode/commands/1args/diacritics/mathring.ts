import {lookupOrAppend} from "../../command-expander";

export const ringOverLetter = lookupOrAppend({
    a: "å",
    A: "Å",
    y: "ẙ"
}, "\u030A");