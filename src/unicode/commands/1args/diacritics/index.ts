import {ringOverLetter} from "./mathring";
import {acuteAccent} from "./acute";
import {graveAccent} from "./grave";
import {circumflex} from "./circumflex";
import {tilde} from "./tilde";
import {dieresis} from "./dieresis";
import {cedilla} from "./cedilla";
import {caron} from "./caron";

import {simpleSuffix} from "../../../../util";
import {ogonek} from "./ogonek";
import {tieLetters} from "./tie-letters";
import {vectorArrow} from "./vectorArrow";
import {longHungarianUmlaut} from "./long-hungarian-umlaut";

export const barUnderLetter = simpleSuffix("\u0331");
export const dotUnderLetter = simpleSuffix("\u0323");
export const breve = simpleSuffix("\u0306");
export const macrron = simpleSuffix("\u0304");
export const dotOverLetter = simpleSuffix("\u0307");
/**
 * For example, \`o → ò
 */
export const modifiersTextModeUnicodeChart = {
    // Text mode
    //
    "`": graveAccent, //{o}	ò	grave accent
    "'": acuteAccent, // Acute accent
    "^": circumflex, //{o}	ô	circumflex
    "~": tilde, //{o}	õ	tilde
    "=": macrron, //{o}	ō	macron accent (a bar over the letter)
    ".": dotOverLetter, //{o}	ȯ	dot over the letter
    '"': dieresis, //{o}	ö	umlaut, trema or dieresis
    "H": longHungarianUmlaut, //{o}	ő	long Hungarian umlaut (double acute)
    "c": cedilla, //{c}	ç	cedilla
    "k": ogonek, //{a}	ą	ogonek
    "b": barUnderLetter, //{o}	o	bar under the letter
    "d": dotUnderLetter, //{u}	ụ	dot under the letter\
    "r": ringOverLetter, //{a}	å	ring over the letter (for å there is also the special command \aa)
    "u": breve, //{o}	ŏ	breve over the letter
    "v": caron, //{s}	š	caron/háček ("v") over the letter

    "t": tieLetters,
};

/**
 * For example, \breve o → ò
 *
 * todo finish
 */
export const modifiersMathModeUnicodeChart = {
    // Math mode
    //
    // \hat{o}	{\displaystyle {\hat {o}}} \hat{o}	circumflex	\^
    // \widehat{oo}	{\displaystyle {\widehat {oo}}} \widehat{oo}	wide version of \hat over several letters
    // \check{o}	{\displaystyle {\check {o}}} \check{o}	vee or check	\v
    "check": caron, //{s}	š	caron/háček ("v") over the letter
    // \tilde{o}	{\displaystyle {\tilde {o}}} \tilde{o}	tilde	\~
    // \widetilde{oo}	{\displaystyle {\widetilde {oo}}} \widetilde{oo}	wide version of \tilde over several letters
    // \acute{o}	{\displaystyle {\acute {o}}} \acute{o}	acute accent	\'
    "acute": acuteAccent, // Acute accent
    // \grave{o}	{\displaystyle {\grave {o}}} \grave{o}	grave accent	\`
    "grave": acuteAccent, //{o}	ò	grave accent
    // \dot{o}	{\displaystyle {\dot {o}}} \dot{o}	dot over the letter	\.
    // \ddot{o}	{\displaystyle {\ddot {o}}} \ddot{o}	two dots over the letter (umlaut in text-mode)	\"
    // \breve{o}	{\displaystyle {\breve {o}}} \breve{o}	breve	\u
    "breve": breve, //{o}	ŏ	breve over the letter
    // \bar{o}	{\displaystyle {\bar {o}}} \bar{o}	macron	\=

    "vec": vectorArrow, // {\displaystyle {\vec {o}}} \vec{o}	vector (arrow) over the letter

    "mathring": ringOverLetter, //{a}	å	ring over the letter (for å there is also the special command \aa
};

// export const diacritics: CommandExpander = (command, context) => {
//     const commandName = command.command.name;
//     switch(commandName) {
//         case "`":
//             // const mode: Mode = context.mode;
//             // switch(mode) {
//             //     case "Paragraph":
//             //     case "LR":
//             //     case "Math":
//
//             // Take first token
//             const firstToken: ParameterToken = command.childNode(0);
//             // Take first letter
//             firstToken.
//
//             const translatedFirstToken = modifiers[commandName](firstToken);
//
//                     // Take middle letter
//
//                     return "";
//     }
// };

export type ModifiersTextMode = keyof typeof modifiersTextModeUnicodeChart;
export type ModifiersMathModeUnicode = keyof typeof modifiersMathModeUnicodeChart;

export const diacriticUnicode = (str: string, arg: string): string | undefined => {
    let fun: (undefined | ((k: string) => (string | undefined))) = modifiersTextModeUnicodeChart[<ModifiersTextMode>str];
    if (!fun) fun = modifiersMathModeUnicodeChart[<ModifiersMathModeUnicode>str];
    return fun && fun(arg);
};