// "t": tieLetters, //{oo}	o͡o	"tie" (inverted u) over the two letters
//o͡o

export function tieLetters(chars: string) {
    return tie2Letters(chars.charAt(0), chars.substring(1));
}
export function tie2Letters(a: string, b: string) {
    return a + "͡" + b;
}

export function isTieLetters(cmdName: string): boolean {
    return cmdName === "t";
}