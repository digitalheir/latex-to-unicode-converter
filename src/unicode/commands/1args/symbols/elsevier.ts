export const elsevierGlyphsUnicodeChart = {
    "2129": "℩",
    "21B3": "↳",
    "2232": "∲",
    "2233": "∳",
    "2238": "∸",
    "2242": "≂",
    "225A": "≚",
    // "225A": "⩣",
    "225F": "≟",
    "2274": "≴",
    "2275": "≵",
    "22C0": "⋀",
    "22C1": "⋁",
    "E838": "⌽",
    "E381": "▱",
    "E212": "⤅",
    "E20C": "⤣",
    "E20D": "⤤",
    "E20B": "⤥",
    "E20A": "⤦",
    "E211": "⤧",
    "E20E": "⤨",
    "E20F": "⤩",
    "E210": "⤪",
    "E21C": "⤳",
    "E21D": "⤳",
    "E21A": "⤶",
    "E219": "⤷",
    "E214": "⥼",
    "E215": "⥽",
    "E291": "⦔",
    "E260": "⦵",
    "E61B": "⦶",
    "E372": "⧜",
    "E395": "⨐",
    "E25A": "⨥",
    "E25B": "⨪",

    "E25C": "⨭",
    "E25D": "⨮",
    "E25E": "⨴",
    "E25F": "⨵", // TODO: according to XML this is E25E, change in https://digitalheir.github.io/mathy-unicode-characters

    "E259": "⨼",
    "E36E": "⩕",
    "E30D": "⫫",
    "300A": "《",
    "300B": "》",
    // "3018": "⦅",
    "3018": "〘",
    "3019": "〙",
};

export type ElsevierGlyph = keyof typeof elsevierGlyphsUnicodeChart;

export function isElsevierGlyph(x: string): x is ElsevierGlyph {
    return elsevierGlyphsUnicodeChart.hasOwnProperty(x);
}

export const translateCharToElsevier: (s: string) => string | undefined = (char) => isElsevierGlyph(char) ? elsevierGlyphsUnicodeChart[char] : undefined;
