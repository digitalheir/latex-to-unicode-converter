export const runesMap = {
    "ra": {
        "\\ae": "ᚨ",
        "\\c": {
            unicode: "ᚳ",
            note: "approximation",
        },
        "\\d": {
            unicode: "ᛞ",
            note: "approximation",
        },
        "\\ea": "ᛠ",
        "\\G": "ᚸ",
        "\\g": {
            unicode: "ᚸ",
            note: "approximation",
        },
        "\\h": "ᚬ",
        "\\j": "ᛄ",
        "\\k": "ᛤ",
        "\\ng": "ᛝ",
        "\\OE": {
            unicode: "ᛟ",
            note: "approximation",
        },
        "\\oe": "ᛟ",
        "\\rex": {
            unicode: "𐎟",
            note: "approximation",
        },
        "\\seight": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sfive": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sfour": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sseven": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\ssix": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\stan": {
            unicode: "ᛥ",
            note: "approximation",
        },
        "\\STAN": "ᛥ",
        "\\sthree": "ᛊ",
        "\\th": "ᚦ",
        "\\y": {
            unicode: "ᚤ",
            note: "approximation",
        },
        "a": "ᚪ",
        "B": {
            unicode: "ᛒ",
            note: "approximation",
        },
        "b": "ᛒ",
        "c": "ᚳ",
        "D": {
            unicode: "ᛞ",
            note: "approximation",
        },
        "d": "ᛞ",
        "e": "ᛖ",
        "F": {
            unicode: "ᚠ",
            note: "approximation",
        },
        "f": "ᚠ",
        "g": "ᚷ",
        "H": {
            unicode: "ᚺ",
            note: "approximation",
        },
        "h": "ᚻ",
        "i": "ᛁ",
        "I": {
            unicode: "ᛇ",
            "latex2": "\\\"i"
        },
        "\\\"i": "ᛇ",
        "J": {
            unicode: "+",
            note: "approximation",
        },
        "j": "ᚼ",
        "k": "ᚲ",
        "l": "ᛚ",
        "m": "ᛗ",
        "n": "ᚾ",
        "o": "ᚩ",
        "P": {
            unicode: "⥏",
            note: "approximation",
        },
        "p": "ᛈ",
        "q": "ᛢ",
        "r": "ᚱ",
        "S": {
            unicode: "⦚",
            note: "approximation",
        },
        "s": "ᛋ",
        "t": "ᛏ",
        "U": {
            unicode: "Λ",
            note: "approximation",
        },
        "u": "ᚢ",
        "w": "ᚹ",
        "x": "ᛉ",
        "Y": {
            unicode: "ᚥ",
            note: "approximation",
        },
        "y": {
            unicode: "ᚣ",
            note: "approximation",
        }
    },
    "rc": {
        "\\A": "ᚼ",
        "\\a": {
            unicode: "┝",
            note: "approximation",
        },
        "\\d": {
            unicode: "⋈",
            note: "approximation",
        },
        "\\h": "ᚻ",
        "\\i": {
            unicode: "⥍",
            note: "approximation",
        },
        "\\ing": {
            unicode: "ᛄ",
            note: "approximation",
        },
        "\\Ing": {
            unicode: "ᛄ",
            note: "approximation",
        },
        "\\ING": "ᛄ",
        "\\j": {
            unicode: "ᛃ",
            note: "approximation",
        },
        "\\k": {
            unicode: "⌵",
            note: "approximation",
        },
        "\\K": {
            unicode: "Υ",
            note: "approximation",
        },
        "\\ng": "ᛜ",
        "\\NG": {
            unicode: "⸋",
            note: "approximation",
        },
        "\\p": {
            unicode: "ᛒ", note: "but with dot in lower"
        },
        "\\R": "ᛦ",
        "\\RR": {
            unicode: "ᛯ",
            note: "approximation",
        },
        "\\s": "ᛋ",
        "\\S": "ᛋ",
        "\\seight": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sfive": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sseven": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\sthree": {
            unicode: "⦚",
            note: "approximation",
        },
        "\\th": "ᚦ",
        "a": "ᚨ",
        "A": "ᛋ",
        "b": "ᛒ",
        "B": {
            unicode: "ᛒ",
            note: "approximation",
        },
        "d": "ᛞ",
        "D": {
            unicode: "▯",
            note: "approximation",
        },
        "e": "ᛖ",
        "E": {
            unicode: "⨅",
            note: "approximation",
        },
        "f": {
            unicode: "ᚠ",
            note: "but skinny"
        },
        "F": "ᚠ",
        "g": "ᚷ",
        "h": "ᚺ",
        "H": {
            unicode: "𝖭",
            note: "approximation",
        },
        "i": "ᛁ",
        "I": {
            unicode: "⥌",
            note: "approximation",
        }, "\\\"i": {
            unicode: "⥌",
            note: "approximation",
        },
        "j": "ᛃ",
        "J": {
            unicode: "ϟ",
            note: "but with box"
        },
        "k": "ᚲ",
        "K": {
            unicode: "Ⴤ",
            note: "approximation",
        },
        "l": "ᛚ",
        "m": "ᛗ",
        "n": "ᚾ",
        "o": "ᛟ",
        "p": "ᛈ",
        "P": {
            unicode: "P",
            note: "approximation",
        },
        "r": "ᚱ",
        "R": "ᛉ",
        "s": "ᛊ",
        "\\sfour": "ᛊ",
        "S": {
            unicode: "⦚",
            note: "approximation",
        },
        "t": "ᛏ",
        "T": {
            unicode: "ᛏ",
            note: "approximation",
        },
        "u": "ᚢ",
        "U": {
            unicode: "Λ",
            note: "approximation",
        },
        "w": "ᚹ"
    },
    "rm": {
        "\\a": "ᛆ", ".a": "ᛆ",
        "\\adot": {
            note: "ᛂ+ᛅ",
        }, "'a": {
            note: "ᛂ+ᛅ",
        },
        "\\c": "ᛍ", ".c": "ᛍ",
        "\\D": {
            note: "Arrow with two dots",
        }, "T": {
            note: "Arrow with two dots",
        },
        "\\e": {
            unicode: "⟊",
            note: "approximation",
        }, "=i": {
            unicode: "⟊",
            note: "approximation",
        },
        "\\g": {
            unicode: "?",
            note: "approximation",
        }, "=k": {
            unicode: "?",
            note: "approximation",
        },
        "\\h": {
            unicode: "⚹",
            note: "approximation",
        },
        "\\l": "ᛛ", ".l": "ᛛ",
        "\\lbar": {
            note: "ᛚ with bar",
        }, "=l": {
            note: "ᛚ with bar",
        },
        "\\ldot": {
            note: "ᛚ with dot",
        }, "'l": {
            note: "ᛚ with dot",
        },
        "\\lflag": {
            note: "ᛚ with flag",
        }, "~l": {
            note: "ᛚ with flag",
        },
        "\\lring": {
            note: "ᛚ with ring",
        }, "^l": {
            note: "ᛚ with ring",
        },
        "\\m": {
            unicode: "ᚴ",
            note: "but mirrored"
        },
        "\\n": {
            unicode: "ᛀ",
            note: "approximation",
        }, ".n": {
            unicode: "ᛀ",
            note: "approximation",
        },
        "\\N": "ᛀ", ".N": "ᛀ",
        "\\ndot": {
            note: "ᚿ+ᛀ",
        }, "'n": {
            note: "ᚿ+ᛀ",
        },
        "\\p": {
            unicode: "ᛒ",
            note: "but with dot in lower"
        }, ".b": {
            unicode: "ᛒ",
            note: "but with dot in lower"
        },
        "\\P": "ᛕ",
        "\\Pdots": {
            note: "ᛕ with dots",
        },
        "\\q": {
            note: "ᚴ mirrored",
        },
        "\\Q": {
            note: "ᛕ mirrored",
        },
        "\\r": {
            unicode: "?",
            note: "approximation",
        },
        "\\rdot": {
            note: "ᚱ with dot",
        }, ".r": {
            note: "ᚱ with dot",
        },
        "\\tbar": {
            unicode: "ᛑ",
            note: "but with bar"
        }, "=t": {
            unicode: "ᛑ",
            note: "but with bar"
        },
        "\\tdot": {
            unicode: "ᛑ",
            note: "but dot translated upper left"
        }, "'t": {
            unicode: "ᛑ",
            note: "but dot translated upper left"
        },
        "\\tflag": {
            unicode: "ᛑ",
            note: "but with flag"
        }, "~t": {
            unicode: "ᛑ",
            note: "but with flag"
        },
        "\\tring": {
            unicode: "ᛑ",
            note: "but with ring"
        }, "^t": {
            unicode: "ᛑ",
            note: "but with ring"
        },
        "a": "ᛆ",
        "A": {
            unicode: "ᛆ",
            note: "but with bigger leg"
        },
        "b": "ᛒ",
        "B": {
            unicode: "ᛒ",
            note: "but like b"
        },
        "c": "ᛌ",
        "C": {
            unicode: "ᛍ",
            note: "approximation",
        }, "^c": {
            unicode: "ᛍ",
            note: "approximation",
        },
        "d": "ᛑ", ".t": "ᛑ",
        "D": {
            note: "ᛏ+ᛂ",
        }, ".T": {
            note: "ᛏ+ᛂ",
        },
        "e": "ᛂ", ".i": "ᛂ",
        "E": {
            unicode: "ᛂ",
            note: "but with ring"
        }, "^i": {
            unicode: "ᛂ",
            note: "but with ring"
        },
        "f": "ᚠ",
        "F": {
            note: "ᚠ with extra branch",
        },
        "g": "ᚵ", ".k": "ᚵ",
        "G": {
            unicode: "ᚵ",
            note: "but dot translated bottom left"
        }, "'k": {
            unicode: "ᚵ",
            note: "but dot translated bottom left"
        },
        "h": "ᚼ",
        "H": {
            unicode: "✳",
            note: "approximation",
        },
        "i": "ᛧ",
        "k": "ᚴ",
        "l": "ᛚ",
        "m": "ᛘ",
        "n": "ᚿ",
        "N": "ᚾ",
        "o": "ᚮ",
        "p": "ᛔ",
        "P": "ᚹ",
        "q": {
            note: "ᚹ mirrored",
        },
        "r": "ᚱ"
    },
    "rn": {
        ".i": "ᛂ", "e": "ᛂ",
        ".k": "ᚵ", "g": "ᚵ",
        ".u": "ᚤ", "y": "ᚤ",
        "\\m": {
            note: "ᛘ with dots",
        },
        "M": {
            note: "⫯ with plus",
        }, "\\M": {
            note: "⫯ with plus",
        },
        "\\A": "ᚬ",
        "\\bar": {
            unicode: "❘",
            note: "approximation",
        }, "!": {
            unicode: "❘",
            note: "approximation",
        },
        "\\cross": "⨯", "*": "⨯",
        "\\dot": "᛫", ".": "᛫",
        "\\doublebar": "¦",
        "\\doublecross": {
            note: "two stacked x's",
        },
        "\\doubledot": "᛬", ":": "᛬",
        "\\doubleeye": "᛬",
        "\\doubleplus": {
            unicode: "‡",
            note: "approximation",
        },
        "\\eye": "᛫",
        "\\pentdot": {
            unicode: "⹘",
            note: "proposal",
        },
        "\\penteye": "⸭",
        "\\plus": "᛭", "+": "᛭",
        "\\quaddot": "⁞",
        "\\quadeye": {
            unicode: "⁘",
            note: "approximation",
        },
        "\\star": {
            unicode: "*",
            note: "approximation",
        },
        "\\th": "ᚦ",
        "\\TH": {
            unicode: "ᚦ",
            note: "approximation",
        },
        "\\triplebar": "┆",
        "\\triplecross": {
            note: "three stacked x's",
        },
        "\\tripledot": "⁝",
        "\\tripleeye": "⋮",
        "\\tripleplus": {
            note: "three stacked plusses",
        },
        "A": "ᚭ",
        "a": "ᛅ",
        "b": "ᛒ",
        "B": {
            unicode: "ᛒ",
            note: "but skinny"
        },
        "f": "ᚠ",
        "F": {
            unicode: "ᚠ",
            note: "but skinny"
        },
        "h": "ᚼ",
        "i": "ᛁ",
        "k": "ᚴ",
        "l": "ᛚ",
        "m": "ᛘ",
        "n": "ᚾ",
        "r": "ᚱ",
        "R": "ᛣ",
        "s": "ᛋ",
        "S": {
            unicode: "𝗁",
            note: "but with sharp corner",
        },
        "t": "ᛏ",
        "u": {
            "latex1": "u"
        }
    },
    "rt": {
        //     {
        //         "latex1": "\\A"
        //     },
        //     {
        //         "latex1": "\\b"
        //     },
        //     {
        //
        //         "latex1": "\\h"
        //     },
        //     {
        //         "latex1": "\\l"
        //     },
        //     {
        //         "latex1": "\\m"
        //     },
        //     {
        //         "latex1": "\\R"
        //     },
        //     {
        //
        //         "latex1": "\\s"
        //     },
        //     {
        //         "latex1": "\\t"
        //     },
        //     {
        //         "latex1": "\\th"
        //     },
        //     {
        //         "latex1": "\\TH",
        //         "latex2": "\\texttt{\\TH}"
        //     },
        //     {
        //         "latex1": "A"
        //     },
        //     {
        //         "latex1": "a"
        //     },
        //     {
        //         "latex1": "b"
        //     },
        //     {
        //
        //         "latex1": "f"
        //     },
        //     {
        //         "latex1": "F"
        //     },
        //     {
        //         "latex1": "h"
        //     },
        //     {
        //         "latex1": "i"
        //     },
        //     {
        //
        //         "latex1": "k"
        //     },
        //     {
        //         "latex1": "l"
        //     },
        //     {
        //         "latex1": "L"
        //     },
        //     {
        //         "latex1": "m"
        //     },
        //     {
        //
        //         "latex1": "M"
        //     },
        //     {
        //         "latex1": "n"
        //     },
        //     {
        //         "latex1": "r"
        //     },
        //     {
        //         "latex1": "R"
        //     },
        //     {
        //
        //         "latex1": "s"
        //     },
        //     {
        //         "latex1": "t"
        //     },
        //     {
        //         "latex1": "T"
        //     },
        //     {
        //         "latex1": "u"
        //     },
        //     {
        //
        //         "latex1": "U"
        //     }
    },
    "rl": {
        "\\A": {
            unicode: "ߌ",
            note: "approximation"
        },
        "\\th": "ᛧ",
        "A": {
            unicode: "`",
            note: "approximation"
        },
        "a": {
            unicode: "´",
            note: "approximation"
        },
        "b": {
            unicode: "´",
            note: "approximation"
        },
        "f": {
            unicode: "ߌ",
            note: "approximation"
        },
        "h": "ᛙ",
        "i": "ᛁ",
        "k": "ᛍ",
        "l": {
            unicode: "`",
            note: "approximation"
        },
        "m": {
            unicode: ":",
            note: "approximation"
        },
        "n": {
            unicode: "`",
            note: "approximation"
        },
        "r": {
            unicode: "⎧",
            note: "approximation"
        },
        "R": {
            unicode: ":",
            note: "approximation"
        },
        "s": "ᛧ",
        "t": {
            unicode: "´",
            note: "approximation"
        },
        "u": {
            unicode: "⎫",
            note: "approximation"
        }
    }
};

export type RuneType = keyof typeof runesMap;
export const isRuneType = (x: string): x is RuneType => runesMap.hasOwnProperty(x);

export const runeUnicode = (type: string, innerLatex: string): string | undefined => {
    const runesType = runesMap[type as RuneType];
    if (!runesType) return undefined;

    const found = (runesType as any)[innerLatex];
    if (typeof found === "string") return found;
    if (found.unicode) return found.unicode;
    return undefined;
};