/**
 * For example, \i → ı
 */
export const specialChars = {
    "i": "ı",
    "j": "ȷ",
    "oe": "œ",
    "OE": "Œ",
    "ae": "æ",
    "AE": "Æ",
    "aa": "å",
    "AA": "Å",
    "o": "ø",
    "O": "Ø",
    "ss": "ß",
    "l": "ł",
    "L": "Ł"
};

export type SpecialChar = (keyof typeof specialChars);

export function isSpecialChar(c: string): c is SpecialChar {
    return specialChars.hasOwnProperty(c);
}
