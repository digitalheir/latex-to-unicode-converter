import {
    isTeXComm,
    TeXChar,
    TeXComm,
    LaTeX,
    TeXArg,
    isTeXRaw, isFixArg,
    newFixArg,
    newTeXRaw,
    isTeXChar,
    isArray,
    isTextHaving,
    isOptArg,
    isTeXMath, isSubOrSuperSymbol, newTeXComm, isSubOrSuperScript, SubOrSuperSymbol
} from "latex-parser";
import {CommandOptions} from "../options";
import {unknownCommandError} from "../unknown-command";
import {expand0argsCommand} from "./commands/0args/index";
import {ArgumentNeeded, createCommandHandler} from "../tex/KnownCommand";
import {expand1argsCommand} from "./commands/1args/index";
import {is1argsCommand} from "../latex/commands/1args/index";
import {convertSqrtToUnicode} from "./commands/sqrt/sqrt";
import {expand2argsCommand} from "./commands/2args/index";
import {is2argsCommand} from "../latex/commands/2args/index";
import {convertToTeXCharsDefault} from "latex-parser/ts-compiled/Text/TeX/CategoryCode";


/*
 * when TeX is looking for an argument to a macro defined with \newcommand, if it finds a left brace {, then the
 * argument will be whatever is between this brace and the corresponding } (at the same level); otherwise the first
 * token (character or control sequence) will be the argument.
 **/
export interface ConversionResult {
    result: string;
    blockIndex: number;
}

function isString(x: any): x is string {
    return typeof x === "string";
}

function convertChars(blockIndex: number, latex: (LaTeX | TeXArg)[]): ConversionResult {
    const start = blockIndex;

    do blockIndex++;
    while (isTeXChar(latex[blockIndex]));

    const chars = <any[]>latex.slice(start, blockIndex);
    const result = chars.map(s => s.string).join("");

    return {
        result,
        blockIndex
    };
}


const regexStartingWhitespace = /^\s*/;

function convertTeXCommand(options: CommandOptions,
                           blockIndex: number,
                           latex: (LaTeX | TeXArg)[],
                           current: TeXComm): ConversionResult {
    const value: string | ArgumentNeeded = convertCommand(options, current);
    if (isString(value)) {
        return {
            result: value,
            blockIndex: blockIndex + 1
        };
    } else {

        // Needs an argument...

        const gobbledArguments: (LaTeX | TeXArg)[] = [];
        const rest: string[] = [];

        while (gobbledArguments.length < value.argumentCount && blockIndex < latex.length - 1) {
            blockIndex++;
            const nextWordToGobble = blockIndex;
            if (latex.length < nextWordToGobble - 1)
                throw new Error("Could not gobble " + value.argumentCount + " arguments for " + current.name);
            const nextLaTeXBlock = latex[nextWordToGobble];

            if (isTeXRaw(nextLaTeXBlock)) {
                const whitespaces = /\s+/g;

                const followingText = nextLaTeXBlock.text.replace(regexStartingWhitespace, "");
                let restIndex = -1;
                let lastIndex = 0;
                while (gobbledArguments.length < value.argumentCount) {
                    const argMatch = whitespaces.exec(followingText);
                    if (argMatch) {
                        const match: string = followingText.substring(lastIndex, argMatch.index);
                        restIndex = argMatch.index;
                        lastIndex = argMatch.index + match.length;
                        gobbledArguments.push(...convertToTeXCharsDefault(match));
                    } else {
                        gobbledArguments.push(...convertToTeXCharsDefault(followingText));
                        break;
                    }
                }
                if (restIndex >= 0)
                    rest.push((followingText.substring(restIndex)));
            } else
                gobbledArguments.push(nextLaTeXBlock);
        }
        blockIndex++;

        const argumentsToApply = gobbledArguments
            .map(lll => convertLaTeXBlocksToUnicode(options, [lll]).result/*.trim() ignore spaces */)
            .map(newTeXRaw)
            .map(latex => newFixArg([latex]));

        if (argumentsToApply.length < value.argumentCount) throw new Error(`Could not find enough arguments for command \\${value.name}. Expected ${value.argumentCount}, but found ${argumentsToApply.length}`);

        if (!value.apply) throw new Error("Can't apply " + JSON.stringify(value));

        const result: string[] = [
            value.apply(
                () => {
                },
                argumentsToApply)
        ];

        if (rest.length > 0)
            result.push(rest.join(""));

        return {
            result: result.join(""),
            blockIndex
        };
    }
}

export function isTeXChar2(x: any): x is TeXChar {
    return x !== undefined
        && typeof x.string === "string"
        && typeof x.category === "number";
}

export function convertLaTeXBlocksToUnicode(options: CommandOptions,
                                            latex: (LaTeX | TeXArg)[]): ConversionResult {
    let blockIndex = 0;
    if (latex.length <= 0) return {
        result: "",
        blockIndex
    };

    const finalConversion: string[] = [];

    while (blockIndex < latex.length) {
        const l: LaTeX | TeXArg = latex[blockIndex];
        try {
            if (isTeXChar2(l)) {
                const convertedChars = convertChars(blockIndex, latex);
                blockIndex = convertedChars.blockIndex;
                finalConversion.push(convertedChars.result);
            } else if (isTeXComm(l)) {
                const res = convertTeXCommand(options, blockIndex, latex, l);
                blockIndex = res.blockIndex;
                finalConversion.push(res.result);
            } else if (isFixArg(l) || isOptArg(l)) {
                const res = convertLaTeXBlocksToUnicode(options, l.latex);
                finalConversion.push(res.result);
                blockIndex++;
            } else if (isTextHaving(l)) {
                finalConversion.push(l.text);
                blockIndex++;
            } else if (isTeXMath(l)) {
                return convertLaTeXBlocksToUnicode(options, l.latex);
            } else if (isTeXRaw(l)) {
                finalConversion.push(l.text);
                blockIndex++;
            } else if (isSubOrSuperScript(l)) {
                // TODO handle better
                const args = l.arguments ? l.arguments : [];
                const res = convertTeXCommand(
                    options,
                    blockIndex,
                    latex,
                    newTeXComm(
                        l.type === SubOrSuperSymbol.SUB ? "mathsubscript" : "mathsuperscript",
                        ...args
                    )
                );
                blockIndex = res.blockIndex;
                finalConversion.push(res.result);
            } else if (isArray(l)) {
                const res = convertLaTeXBlocksToUnicode(options, l);
                finalConversion.push(res.result);
                blockIndex++;
            } else {
                // noinspection ExceptionCaughtLocallyJS
                throw new Error("Can't handle LaTeX block yet: " + (JSON.stringify(l)) + ". Leave an issue at https://github.com/digitalheir/tex-to-unicode/issues");
            }
        } catch (e) {
            if (options.onError !== undefined) {
                const saved = options.onError(e, l);
                if (saved !== undefined) {
                    finalConversion.push(saved);
                    blockIndex++;
                } else throw e;
            } else throw e;
        }
    }

    return {
        result: finalConversion.join(""),
        blockIndex
    };
}

// export declare type LaTeXRaw = TeXBuildingBlocks | TeXRaw;
// export declare type LaTeXNoRaw = TeXBuildingBlocks | TeXChar;
// export declare type TeXBuildingBlocks = TeXComm | TeXEnv | TeXMath | TeXLineBreak | TeXBraces | TeXComment;

// export function convertLaTeXBlockToUnicode(latex: LaTeX,
//                              options: CommandOptions = {
//                                  translateTo: "unicode"
//                              }): ConversionResult {
//     if (isTeXComm(latex)) {
//         convertCommand(latex);
//     } else {
//         return latex.toString();
//     }
// }

function convert1ArgCommand(options: CommandOptions, cmd: TeXComm): string | ArgumentNeeded {
    if (cmd.arguments.length > 0) {
        const expanded: string = expand1argsCommand(
            cmd.name,
            convertLaTeXBlocksToUnicode(options, [cmd.arguments[0]]).result || ""
        );
        if (cmd.arguments.length > 1)
            return expanded + convertLaTeXBlocksToUnicode(options, cmd.arguments.slice(1)).result;
        else
            return expanded;
    }
    else
        return createCommandHandler(
            cmd.name,
            0,
            1,
            (cb, texArgs) => {
                const firstArg = texArgs[0];
                const rest = texArgs.slice(1);
                const a = expand1argsCommand(
                    cmd.name,
                    convertLaTeXBlocksToUnicode(options, [firstArg]).result
                );
                const b = convertLaTeXBlocksToUnicode(options, rest).result;
                return a + b;
            });
}

function convert2ArgCommand(options: CommandOptions, cmd: TeXComm): string | ArgumentNeeded {
    if (cmd.arguments.length > 1) {
        const expanded: string = expand2argsCommand(
            cmd.name,
            convertLaTeXBlocksToUnicode(options, [cmd.arguments[0]]).result || "",
            convertLaTeXBlocksToUnicode(options, [cmd.arguments[1]]).result || ""
        );
        if (cmd.arguments.length > 2)
            return expanded + convertLaTeXBlocksToUnicode(options, cmd.arguments.slice(1)).result;
        else
            return expanded;
    }
    else
        return createCommandHandler(
            cmd.name,
            0,
            2,
            (cb, texArgs) => {
                const firstArg = texArgs[0];
                const secondArg = texArgs[1];
                const rest = texArgs.slice(2);
                const a = expand2argsCommand(
                    cmd.name,
                    convertLaTeXBlocksToUnicode(options, [firstArg]).result,
                    convertLaTeXBlocksToUnicode(options, [secondArg]).result
                );
                const b = convertLaTeXBlocksToUnicode(options, rest).result;
                return a + b;
            });
}

function convertSqrt(options: CommandOptions, cmd: TeXComm) {
    let base = undefined;
    let nucleus = undefined;
    let argIndex = 0;
    while (nucleus === undefined && argIndex < cmd.arguments.length) {
        const arg = cmd.arguments[argIndex];
        const argAsString = convertLaTeXBlocksToUnicode(options, [arg]).result;
        if (isOptArg(arg) && !base) {
            base = argAsString;
        } else {
            nucleus = argAsString;
        }
        argIndex++;
    }
    if (nucleus)
        return convertSqrtToUnicode(nucleus, base);
    else
        return createCommandHandler(
            cmd.name,
            1,
            1,
            (cb, texArgs) => {
                const firstArg = texArgs[0];
                const a = convertSqrtToUnicode(
                    convertLaTeXBlocksToUnicode(options, [firstArg]).result
                );
                if (texArgs.length > 1) {
                    const rest = texArgs.slice(1);
                    const b = convertLaTeXBlocksToUnicode(options, rest).result;
                    return a + b;
                }
                return a;
            });
}

export function convertCommand(options: CommandOptions, cmd: TeXComm): string | ArgumentNeeded {
    const commandName = cmd.name;

    const expanded0args = expand0argsCommand(commandName);
    if (!!expanded0args)
        if (cmd.arguments && cmd.arguments.length > 0)
            return expanded0args + convertLaTeXBlocksToUnicode(options, cmd.arguments).result;
        else
            return expanded0args;
    else if (is1argsCommand(commandName)) {
        return convert1ArgCommand(options, cmd);
    } else if (is2argsCommand(commandName)) {
        return convert2ArgCommand(options, cmd);
    } else if (commandName === "sqrt") {
        return convertSqrt(options, cmd);
    } else {
        throw unknownCommandError(commandName);
    }
}

// export function apply(command: string,
//                       args: TeXArg[],
//                       cb: (res: string, err: Error) => any): string {
//     if (unicodeSupportedCommands.hasOwnProperty(command)) {
//         if (diacritics.hasOwnProperty(command)) {
//             try {
//                 diacritics[command](parameter)
//             } catch (e) {
//                 cb(parameter, e);
//             }
//         } else {
//             cb(parameter, new Error("I do not know how to apply command: " + command + ". "
//                 + "Please submit a feature request at https://github.com/digitalheir/tex-to-unicode/issues."));
//         }
//     }
// }