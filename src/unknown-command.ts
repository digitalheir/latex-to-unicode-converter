export function unknownCommandError(cmd: string): Error {
    return new Error("I do not know command " + cmd);
}