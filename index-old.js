// Load all of our data
var accents = require('./src/unicode/accents');
var aliases = require('./data/aliases');
var subscripts = require('./data/subscripts');
var superscripts = require('./data/superscripts');
var symbols = require('./data/symbols');
var textbb = require('./data/textbb');
var textbf = require('./data/textbf');
var textcal = require('./data/textcal');
var textfrak = require('./data/textfrak');
var textit = require('./data/textit');
var textmono = require('./data/textmono');

// Replace all occurences of `search` by `replacement` within the string
// Its use RegExp because `g` flag is not supported in Node
// Moreover, it has to escape the reserved char of `search` 
function replaceAll(search, replacement) {
  search = search.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
  return this.replace(new RegExp(search, 'g'), replacement);
}

// Insert `str` anywhere in the string
function insert(index, str) {
  if (index <= 0) return str + this;
  var stringLeft = this.substring(0, index);
  var stringRight = this.substring(index, this.length);
  return stringLeft + str + stringRight;
}

// Replace some latex symbols with their alias equivalent
function convertAliases(str) {
  for (var i = 0; i < aliases.length; i++) {
    str = replaceAll.call(str,aliases[i][0], aliases[i][1]);
  }
  return str;
}

// Replace each '\alpha', '\beta' and similar latex symbols with
// their unicode representation.
function convertLatexSymbols(str) {
  for (var i = 0; i < symbols.length; i++) {
    str = replaceAll.call(str, symbols[i][0], symbols[i][1]);
  }
  return str;
}

// Example: modifier = '^', D = superscripts
// This will search for the ^ signs and replace the next
// digit or (digits when {} is used) with its/their uppercase representation
function applyModifier(text, modifier, D) {
  //text = replaceAll.call(text, modifier, '^');
  var newtext = '';
  var modeNormal = 0;
  var modeModified = 1;
  var modeLong = 2;

  var mode = modeNormal;
  var ch;
  
  
  let slices = []
  
  outer: for(let offset=0;offset<text.length;) {
    let next = text.indexOf(modifier, offset)
    if(next == -1) {
       slices.push(text.substring(offset))
      break;
    }
    
    if(next > offset) {
      slices.push(text.substring(offset,next))
    }
    
    offset = next + modifier.length;
    
    let char = text[offset] 
    
    if(char && D[char]) {
      slices.push(D[char])
      offset++;
      continue;
    }
    
    let re = /{([^}]+?)}/g;
    re.lastIndex = offset;
    let match = re.exec(text);
    
    if(match) {
      let group = match[1];
      let subslices = [];

      for(let i=0;i<group.length;i++) {
        let groupChar = group[i];
        let replacement = D[groupChar];
        if(!replacement) {
          // failed to transform contents of a bracketed expression
          // -> retain the whole thing verbatim
          slices.push(modifier)
          slices.push(match[0])
          offset = re.lastIndex;
          continue outer;
        }
        
        subslices.push(replacement);
      }
          
      slices.push(...subslices);
      offset = re.lastIndex;
      continue;
    }
    
    // failed to replace, retain modifier
    slices.push(modifier)
      
  }
  
  return slices.join("");
  
}

// Apply all of the modifiers
function applyAllModifiers(str) {
  str = applyModifier(str, '^', superscripts);
  str = applyModifier(str, '_', subscripts);
  str = applyModifier(str, '\\bb', textbb);
  str = applyModifier(str, '\\bf', textbf);
  str = applyModifier(str, '\\it', textit);
  str = applyModifier(str, '\\cal', textcal);
  str = applyModifier(str, '\\frak', textfrak);
  str = applyModifier(str, '\\mono', textmono);
  return str;
}

// Find the content enclosed by brackets
// It will also return the index of the closing bracket associated
// with the opening one starting at position `i` of the string `str`
function parseBracket(str, i, bracket) {
  var open = bracket[0];
  var close = bracket[1];
  var start = i;
  if (str[i] != open) return ['', 0];

  var n = 1;
  var out = '';
  i++;

  while (n > 0 && i < str.length) {
    if (str[i] === open) n++;
    if (str[i] === close) n--;
    if (n != 0 || str[i] != close) out += str[i];
    i++; 
  }

  return [out, i - start];
}

// Enclose mathematical expression `str` within parenthesis
// to ensure the respect of calculations priorities
function addParenthesis(str) {
  if (str.length <= 1 ||
     (str[0] == "(" && str.slice(-1) == ")") ||
     /^[a-z0-9_]*$/i.test(str)) {
    return str;
  }
  return '(' + str + ')';
}

// Convert accents (\a{b})
function convertAccents(str) {
  for (var i = 0; i < accents.length; i++) {
    var key = accents[i][0];
    var value = accents[i][1];

    while (str.indexOf(key) != -1) {
      var idx = str.indexOf(key);
      var parsed = parseBracket(str, idx + key.length, '{}');
      var converted = convertAccents(parsed[0] !== '' ? parsed[0] : ' ');
      converted = insert.call(converted, Math.round(converted.length / 2.0), value);
      converted = JSON.parse('"' + converted + '"');
      var subLeft = str.substring(0, idx);
      var subRight = str.substring(idx + key.length + parsed[1]);
      str = subLeft + converted + subRight;
    }
  }
  return str;
}

// Replace str with unicode representations
module.exports = function(str) {

  str = convertAliases(str);
  str = convertLatexSymbols(str);
  str = applyAllModifiers(str);
  str = convertFrac(str);
  str = convertSqrt(str);
  str = convertBinom(str);
  str = convertAccents(str);

  return str;
};
